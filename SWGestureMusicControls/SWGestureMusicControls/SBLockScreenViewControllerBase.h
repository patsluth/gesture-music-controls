//
//  SBLockScreenViewControllerBase.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@interface SBLockScreenViewControllerBase : UIViewController
{
}

- (void)viewWillDisappear:(BOOL)view;
- (void)viewWillAppear:(BOOL)view;

- (void)setCustomUnlockActionContext:(id)context;
- (id)_customUnlockActionContext;
- (id)currentUnlockActionContext;

- (void)disableLockScreenBundleWithName:(id)name deactivationContext:(id)context;
- (void)enableLockScreenBundleWithName:(id)name activationContext:(id)context;


- (void)setPasscodeLockVisible:(BOOL)visible
                      animated:(BOOL)animated
              withUnlockSource:(int)unlockSource
                    andOptions:(id)options;
- (void)setPasscodeLockVisible:(BOOL)visible animated:(BOOL)animated completion:(id)completion;
- (void)setPasscodeLockVisible:(BOOL)visible animated:(BOOL)animated;

@end




