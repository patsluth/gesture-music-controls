//
//  SBLockScreenManager.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@class SBLockScreenViewControllerBase;

@interface SBLockScreenManager
{
	BOOL _isUILocked;
	SBLockScreenViewControllerBase *_lockScreenViewController;
}

@property(readonly, assign) BOOL isUILocked;
@property(readonly, assign, nonatomic) SBLockScreenViewControllerBase *lockScreenViewController;

+ (id)sharedInstanceIfExists;
+ (id)sharedInstance;
+ (id)_sharedInstanceCreateIfNeeded:(BOOL)needed;

- (void)_removeDisableUnlockAssertion:(id)assertion;
- (void)_addDisableUnlockAssertion:(id)assertion;
- (void)cancelApplicationRequestedDeviceLockEntry;
- (void)applicationRequestedDeviceUnlock;
- (void)removeLockScreenDisableAssertion:(id)assertion;
- (void)addLockScreenDisableAssertion:(id)assertion;
- (void)_lockUI;
- (void)_setUILocked:(BOOL)locked;

@end




