#line 1 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCAppLauncher.xm"








#import "SWGMCAppLauncher.h"

#import "SBApplication.h"
#import "SBUIController.h"
#import "SBDeviceLockController.h"
#import "SBLockScreenManager.h"
#import "SBLockScreenViewControllerBase.h"
#import "SBUnlockActionContext.h"
#import "SBControlCenterController.h"

#include <logos/logos.h>
#include <substrate.h>
@class SBControlCenterController; @class SBUnlockActionContext; @class SBDeviceLockController; @class SBLockScreenManager; @class SBUIController; 

static __inline__ __attribute__((always_inline)) Class _logos_static_class_lookup$SBControlCenterController(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("SBControlCenterController"); } return _klass; }static __inline__ __attribute__((always_inline)) Class _logos_static_class_lookup$SBUnlockActionContext(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("SBUnlockActionContext"); } return _klass; }static __inline__ __attribute__((always_inline)) Class _logos_static_class_lookup$SBDeviceLockController(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("SBDeviceLockController"); } return _klass; }static __inline__ __attribute__((always_inline)) Class _logos_static_class_lookup$SBLockScreenManager(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("SBLockScreenManager"); } return _klass; }static __inline__ __attribute__((always_inline)) Class _logos_static_class_lookup$SBUIController(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("SBUIController"); } return _klass; }
#line 19 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCAppLauncher.xm"
@implementation SWGMCAppLauncher : NSObject 


+ (void)doLaunchApp:(SBApplication *)app {
    if (!app){
        return;
    }
    
	SBUIController *sbUIC = (SBUIController *)[_logos_static_class_lookup$SBUIController() sharedInstanceIfExists];
    
    if (sbUIC){
        [sbUIC activateApplicationAnimated:app];
    }
}


+ (void)launchAppLockScreenFriendly:(SBApplication *)app {
    if (!app){
		return;
    }
    
    SBDeviceLockController *deviceLC = (SBDeviceLockController *)[_logos_static_class_lookup$SBDeviceLockController() sharedController];
    
	if (deviceLC && deviceLC.isPasscodeLocked){
        
		SBLockScreenManager *manager = (SBLockScreenManager *)[_logos_static_class_lookup$SBLockScreenManager() sharedInstance];
        
		if (manager && manager.isUILocked){
            
			void (^action)() = ^(){
                [SWGMCAppLauncher doLaunchApp:app];
			};
            
			SBLockScreenViewControllerBase *controller = [manager lockScreenViewController];
            
            if (controller){
                
                SBUnlockActionContext *context = [[_logos_static_class_lookup$SBUnlockActionContext() alloc] initWithLockLabel:nil
                                                                                       shortLockLabel:nil
                                                                                         unlockAction:action
                                                                                           identifier:nil];
                context.deactivateAwayController = YES;
                [controller setCustomUnlockActionContext:context];
                
                SBControlCenterController *sbCC = (SBControlCenterController *)[_logos_static_class_lookup$SBControlCenterController()
                                                                                sharedInstanceIfExists];
                if (sbCC){
                    [sbCC dismissAnimated:YES];
                }
                
                [controller setPasscodeLockVisible:YES animated:YES completion:nil];
                
                return;
                
            }
		}
	}
    
    [SWGMCAppLauncher doLaunchApp:app];
}

@end




#line 85 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCAppLauncher.xm"
