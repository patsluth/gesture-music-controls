//
//  SWGMCMusicShuffleActivity.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-02-26.
//
//

#import "SWUIActivityWithAction.h"

#define SW_GMC_SHUFFLE_ACTIVITY_TYPE @"com.patsluth.swgesturemusiccontrols.shuffleactivity"

//SHUFFLE MODE DISPLAY VALUES - ACTUAL VALUES as per Music app
//0 - @"Shuffle" - Shuffle Off
//1 - Non existant for some reason
//2 - @"Shuffle All" - Shuffle All

@interface SWGMCMusicShuffleActivity : SWUIActivityWithAction

@property (nonatomic) int currentShuffleMode;

- (id)initWithAction:(swActivityAction)action andShuffleMode:(int)mode;
+ (NSString *)displayStringForCurrentShuffleMode:(int)mode;
+ (NSString *)displayStringForNextShuffleMode:(int)mode;
+ (UIImage *)imageForNextShuffleMode:(int)mode;

@end




