//
//  SWGMCMusicShuffleActivity.m
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-02-26.
//
//

#import "SWGMCMusicShuffleActivity.h"

#define IMAGE_NAME_UIACTIVITY_SHUFFLE_ALL @"SW_GMC_UIActivity_Shuffle_All"

@implementation SWGMCMusicShuffleActivity

#pragma mark Init

- (id)initWithAction:(swActivityAction)action andShuffleMode:(int)mode
{
    self = [super initWithAction:action];
    if (self){
        self.currentShuffleMode = mode;
    }
    return self;
}

#pragma mark Public Methods

//the value which we will be changing to
+ (NSString *)displayStringForCurrentShuffleMode:(int)mode
{
    NSString *returnValue;
    
    if (mode == 0){
        returnValue = @"Shuffle Off";
    } else if (mode == 2){
        returnValue = @"Shuffle All";
    }
    
    return returnValue;
}

+ (NSString *)displayStringForNextShuffleMode:(int)mode
{
    NSString *returnValue;
    
    if (mode == 0){
        returnValue = @"Shuffle All";
    } else if (mode == 2){
        returnValue = @"Shuffle Off";
    }
    
    return returnValue;
}

+ (UIImage *)imageForNextShuffleMode:(int)mode;
{
    UIImage *shuffleImage =  [UIImage imageWithContentsOfFile:[[NSBundle bundleWithPath:SW_GMC_BUNDLE_PATH]
                                                            pathForResource:IMAGE_NAME_UIACTIVITY_SHUFFLE_ALL
                                                               ofType:@"png"]];
    
    if (mode == 2){
        
        UIImage *cross =  [UIImage imageWithContentsOfFile:[[NSBundle bundleWithPath:SW_GMC_BUNDLE_PATH]
                                                                   pathForResource:IMAGE_NAME_UIACTIVITY_CROSS
                                                                   ofType:@"png"]];
        
        UIGraphicsBeginImageContextWithOptions(cross.size, NO, cross.scale);
        
        [cross drawAtPoint: CGPointMake(0,0)];
        
        CGPoint middlePoint = CGPointMake((cross.size.width / 2) - (shuffleImage.size.width / 2),
                                          (cross.size.height / 2) - (shuffleImage.size.height / 2));
        
        [shuffleImage drawAtPoint:middlePoint blendMode:kCGBlendModeNormal alpha:1.0];
        
        UIImage *mergedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return mergedImage;
    }
    
    return shuffleImage;
}

#pragma mark Internal Methods

- (NSString *)activityType
{
    return SW_GMC_SHUFFLE_ACTIVITY_TYPE;
}

- (NSString *)activityTitle
{
    return [SWGMCMusicShuffleActivity displayStringForNextShuffleMode:self.currentShuffleMode];
}

- (UIImage *)activityImage
{
    return [SWGMCMusicShuffleActivity imageForNextShuffleMode:self.currentShuffleMode];;
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    return YES;
}

- (UIViewController *)activityViewController
{
    return nil;
}

@end




