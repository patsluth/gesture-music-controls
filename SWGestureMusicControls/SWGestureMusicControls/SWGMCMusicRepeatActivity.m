//
//  SWGMCMusicRepeatActivity.m
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-02-26.
//
//

#import "SWGMCMusicRepeatActivity.h"

#define IMAGE_NAME_UIACTIVITY_REPEAT_ONE @"SW_GMC_UIActivity_Repeat_One"
#define IMAGE_NAME_UIACTIVITY_REPEAT_ALL @"SW_GMC_UIActivity_Repeat_All"

@implementation SWGMCMusicRepeatActivity

#pragma mark Init

- (id)initWithAction:(swActivityAction)action andRepeatMode:(int)mode
{
    self = [super initWithAction:action];
    if (self){
        self.currentRepeatMode = mode;
    }
    return self;
}

#pragma mark Public Methods

+ (NSString *)displayStringForCurrentRepeatMode:(int)mode
{
    NSString *returnValue;
    
    switch (mode){
        case 0:
            returnValue = @"Repeat Off";
            break;
            
        case 1:
            returnValue = @"Repeat Song";
            break;
            
        case 2:
            returnValue = @"Repeat All";
            break;
            
        default:
            break;
    }
    
    return returnValue;
}

+ (NSString *)displayStringForNextRepeatMode:(int)mode
{
    NSString *returnValue;
    
    switch (mode){
        case 0:
            returnValue = @"Repeat Song";
            break;
            
        case 1:
            returnValue = @"Repeat All";
            break;
            
        case 2:
            returnValue = @"Repeat Off";
            break;
            
        default:
            break;
    }
    
    return returnValue;
}

+ (UIImage *)imageForNextRepeatMode:(int)mode;
{
    UIImage *repeatImage;
    
    if (mode == 0){
        repeatImage =  [UIImage imageWithContentsOfFile:[[NSBundle bundleWithPath:SW_GMC_BUNDLE_PATH]
                                                         pathForResource:IMAGE_NAME_UIACTIVITY_REPEAT_ONE
                                                         ofType:@"png"]];
    } else if (mode == 1){
        repeatImage =  [UIImage imageWithContentsOfFile:[[NSBundle bundleWithPath:SW_GMC_BUNDLE_PATH]
                                                         pathForResource:IMAGE_NAME_UIACTIVITY_REPEAT_ALL
                                                         ofType:@"png"]];
    } else if (mode == 2){
        repeatImage =  [UIImage imageWithContentsOfFile:[[NSBundle bundleWithPath:SW_GMC_BUNDLE_PATH]
                                                         pathForResource:IMAGE_NAME_UIACTIVITY_REPEAT_ALL
                                                         ofType:@"png"]];
        
        UIImage *cross =  [UIImage imageWithContentsOfFile:[[NSBundle bundleWithPath:SW_GMC_BUNDLE_PATH]
                                                            pathForResource:IMAGE_NAME_UIACTIVITY_CROSS
                                                            ofType:@"png"]];
        
        UIGraphicsBeginImageContextWithOptions(cross.size, NO, cross.scale);
        
        [cross drawAtPoint: CGPointMake(0,0)];
        
        CGPoint middlePoint = CGPointMake((cross.size.width / 2) - (repeatImage.size.width / 2),
                                          (cross.size.height / 2) - (repeatImage.size.height / 2));
        
        [repeatImage drawAtPoint:middlePoint blendMode:kCGBlendModeNormal alpha:1.0];
        
        UIImage *mergedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return mergedImage;
    }
    
    return repeatImage;
}

#pragma mark Internal Methods

- (NSString *)activityType
{
    return SW_GMC_REPEAT_ACTIVITY_TYPE;
}

- (NSString *)activityTitle
{
    return [SWGMCMusicRepeatActivity displayStringForNextRepeatMode:self.currentRepeatMode];
}

- (UIImage *)activityImage
{
    return [SWGMCMusicRepeatActivity imageForNextRepeatMode:self.currentRepeatMode];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    return YES;
}

- (UIViewController *)activityViewController
{
    return nil;
}

@end




