//
//  SBDeviceLockController.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@interface SBDeviceLockController
{
}

+ (id)_sharedControllerIfExists;
+ (id)sharedController;
- (BOOL)isPasscodeLocked;

@end




