//
//  MPPlaybackControlsView.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@class MPAVController;
@class MPDetailSlider;

@interface MPPlaybackControlsView : UIView
{
    MPAVController *_player;
	MPDetailSlider *_progressControl;
}

@property(retain) MPAVController *player;

- (void)setCurrentTime:(double)arg1 options:(int)arg2;
- (void)setCurrentTime:(double)arg1;

@end




