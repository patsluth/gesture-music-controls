#line 1 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCMusicApp.xm"








#import "SWGMCSlideView.h"
#import "SWGMCPrefsMusicAppController.h"

#import "MusicNowPlayingViewController.h"
#import "MusicNowPlayingTitlesView.h"
#import "MusicNowPlayingPlaybackControlsView.h"
#import "MPDetailSlider.h"
#import "MusicNowPlayingTransportControls.h"
#import "MPURatingControl.h"
#import "MPVolumeSlider.h"
#import "MPAVController.h"
#import "MPButton.h"
#import "MPAVItem.h"

#pragma mark MusicNowPlayingViewController

#include <logos/logos.h>
#include <substrate.h>
@class MusicNowPlayingViewController; @class MPURatingControl; 
static void (*_logos_orig$_ungrouped$MusicNowPlayingViewController$viewDidLayoutSubviews)(MusicNowPlayingViewController*, SEL); static void _logos_method$_ungrouped$MusicNowPlayingViewController$viewDidLayoutSubviews(MusicNowPlayingViewController*, SEL); static void (*_logos_orig$_ungrouped$MusicNowPlayingViewController$viewWillAppear$)(MusicNowPlayingViewController*, SEL, BOOL); static void _logos_method$_ungrouped$MusicNowPlayingViewController$viewWillAppear$(MusicNowPlayingViewController*, SEL, BOOL); static void (*_logos_orig$_ungrouped$MusicNowPlayingViewController$viewDidAppear$)(MusicNowPlayingViewController*, SEL, BOOL); static void _logos_method$_ungrouped$MusicNowPlayingViewController$viewDidAppear$(MusicNowPlayingViewController*, SEL, BOOL); static void (*_logos_orig$_ungrouped$MusicNowPlayingViewController$willAnimateRotationToInterfaceOrientation$duration$)(MusicNowPlayingViewController*, SEL, int, double); static void _logos_method$_ungrouped$MusicNowPlayingViewController$willAnimateRotationToInterfaceOrientation$duration$(MusicNowPlayingViewController*, SEL, int, double); static void (*_logos_orig$_ungrouped$MusicNowPlayingViewController$didRotateFromInterfaceOrientation$)(MusicNowPlayingViewController*, SEL, int); static void _logos_method$_ungrouped$MusicNowPlayingViewController$didRotateFromInterfaceOrientation$(MusicNowPlayingViewController*, SEL, int); static SWGMCBaseView * _logos_method$_ungrouped$MusicNowPlayingViewController$currentGMC(MusicNowPlayingViewController*, SEL); static MPAVController * _logos_method$_ungrouped$MusicNowPlayingViewController$mpavPlayer(MusicNowPlayingViewController*, SEL); static MPAVItem * _logos_method$_ungrouped$MusicNowPlayingViewController$item(MusicNowPlayingViewController*, SEL); static unsigned long long _logos_method$_ungrouped$MusicNowPlayingViewController$transportVisibleParts(MusicNowPlayingViewController*, SEL); static void (*_logos_orig$_ungrouped$MusicNowPlayingViewController$_setShowingRatings$animated$)(MusicNowPlayingViewController*, SEL, BOOL, BOOL); static void _logos_method$_ungrouped$MusicNowPlayingViewController$_setShowingRatings$animated$(MusicNowPlayingViewController*, SEL, BOOL, BOOL); static void _logos_method$_ungrouped$MusicNowPlayingViewController$startRatingShouldHideTimer(MusicNowPlayingViewController*, SEL); static void _logos_method$_ungrouped$MusicNowPlayingViewController$hideRatingControlWithTimer(MusicNowPlayingViewController*, SEL); static void (*_logos_orig$_ungrouped$MPURatingControl$_handlePanGesture$)(MPURatingControl*, SEL, id); static void _logos_method$_ungrouped$MPURatingControl$_handlePanGesture$(MPURatingControl*, SEL, id); static void (*_logos_orig$_ungrouped$MPURatingControl$_handleTapGesture$)(MPURatingControl*, SEL, id); static void _logos_method$_ungrouped$MPURatingControl$_handleTapGesture$(MPURatingControl*, SEL, id); 

#line 25 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCMusicApp.xm"



static void _logos_method$_ungrouped$MusicNowPlayingViewController$viewDidLayoutSubviews(MusicNowPlayingViewController* self, SEL _cmd) {
    
    SWGMCBaseView *gmc = [self currentGMC];
    if (gmc && gmc.shouldBlockLayout){
        return;
    }
    
    _logos_orig$_ungrouped$MusicNowPlayingViewController$viewDidLayoutSubviews(self, _cmd);
    
    BOOL enabled = [[SWGMCPrefs valueForBaseKey:@"enabled"
                                  forPrefsClass:[SWGMCPrefsMusicAppController class]
                                   defaultValue:@YES] boolValue];
    
    if (!enabled){
        return;
    }
    
    
    MusicNowPlayingTitlesView *title =  MSHookIvar<MusicNowPlayingTitlesView *>(self, "_titlesView");
    MusicNowPlayingPlaybackControlsView *control = MSHookIvar<MusicNowPlayingPlaybackControlsView *>(self,
                                                                   "_playbackControlsView");
    MPDetailSlider *progressControl =  MSHookIvar<MPDetailSlider *>(control, "_progressControl");
    
    MusicNowPlayingTransportControls *transport = MSHookIvar<MusicNowPlayingTransportControls *>(control,
                                                                "_transportControls");
    
    MPURatingControl *rating = MSHookIvar<MPURatingControl *>(self, "_ratingControl");
    MPVolumeSlider *volume = MSHookIvar<MPVolumeSlider *>(control, "_volumeSlider");
    
    BOOL progressHidden = [[SWGMCPrefs valueForBaseKey:@"hideprogress_enabled"
                                         forPrefsClass:[SWGMCPrefsMusicAppController class]
                                          defaultValue:@YES] boolValue];
    BOOL volumeHidden = [[SWGMCPrefs valueForBaseKey:@"hidevolume_enabled"
                                       forPrefsClass:[SWGMCPrefsMusicAppController class]
                                        defaultValue:@YES] boolValue];
    
    
    [control layoutSubviews];
    
    transport.hidden = YES;
    
    if (!gmc){
        
        gmc = [[SWGMCSlideView alloc] init];
        gmc.prefInstanceClass = [SWGMCPrefsMusicAppController class];
        
        gmc.musicAppNowPlayingVC = self;
        
        [control addSubview:gmc];
        [gmc.scrollview addSubview:title];

        gmc.systemRadioLikeOrBanButton = MSHookIvar<MPButton *>(transport, "_likeOrBanButton");
    }
    
    CGRect currentFrame = gmc.frame;
    
    CGFloat gmcHeight = volume.frame.origin.y - progressControl.frame.origin.y - (progressHidden ? 0 : progressControl.frame.size.height) + (volumeHidden ? volume.frame.size.height : 0);
    
    CGRect newFrame = CGRectMake(self.view.frame.origin.x,
                                 progressControl.frame.origin.y + (progressHidden ? 0 : progressControl.frame.size.height),
                                 self.view.frame.size.width,
                                 gmcHeight);
    
    if (!CGRectEqualToRect(currentFrame, newFrame)){
        gmc.frame = newFrame;
        [gmc layoutSubviews];
        
        [gmc resetContentOffset];
    }
    
    
    title.frame = CGRectMake(0,
                             (gmc.scrollview.frame.size.height / 2) - (title.frame.size.height / 2),
                             title.frame.size.width,
                             title.frame.size.height);
    title.center = CGPointMake(gmc.scrollview.contentSize.width / 2, title.center.y);
    
    gmc.nowPlayingTitleView = title; 
    
    
    CGPoint convertedCenter = [gmc.scrollview convertPoint:title.center toView:rating.superview];
    rating.center = convertedCenter;
    
    [control bringSubviewToFront:gmc];
    [gmc.scrollview bringSubviewToFront:title];
    
    [transport.superview bringSubviewToFront:transport];
    [progressControl.superview bringSubviewToFront:progressControl];
    [volume.superview bringSubviewToFront:volume];
    
    if (progressHidden){
        [progressControl removeFromSuperview];
    } else {
        if (!progressControl.superview){
            [gmc.superview addSubview:progressControl];
        }
    }
    
    if (volumeHidden){
        [volume removeFromSuperview];
    } else {
        if (!volume.superview){
            [gmc.superview addSubview:volume];
        }
    }
    
    
    [title setTitleText:title.titleText];
}


static void _logos_method$_ungrouped$MusicNowPlayingViewController$viewWillAppear$(MusicNowPlayingViewController* self, SEL _cmd, BOOL arg1) {
    _logos_orig$_ungrouped$MusicNowPlayingViewController$viewWillAppear$(self, _cmd, arg1);
    
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (gmc){
        [gmc resetContentOffset];
    }
}


static void _logos_method$_ungrouped$MusicNowPlayingViewController$viewDidAppear$(MusicNowPlayingViewController* self, SEL _cmd, BOOL arg1) {
    _logos_orig$_ungrouped$MusicNowPlayingViewController$viewDidAppear$(self, _cmd, arg1);
    
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (gmc){
        [gmc resetContentOffset];
    }
}


static void _logos_method$_ungrouped$MusicNowPlayingViewController$willAnimateRotationToInterfaceOrientation$duration$(MusicNowPlayingViewController* self, SEL _cmd, int arg1, double arg2) {
    _logos_orig$_ungrouped$MusicNowPlayingViewController$willAnimateRotationToInterfaceOrientation$duration$(self, _cmd, arg1, arg2);
    [self viewDidLayoutSubviews];
}


static void _logos_method$_ungrouped$MusicNowPlayingViewController$didRotateFromInterfaceOrientation$(MusicNowPlayingViewController* self, SEL _cmd, int arg1) {
    _logos_orig$_ungrouped$MusicNowPlayingViewController$didRotateFromInterfaceOrientation$(self, _cmd, arg1);
    [self viewDidLayoutSubviews];
}



static SWGMCBaseView * _logos_method$_ungrouped$MusicNowPlayingViewController$currentGMC(MusicNowPlayingViewController* self, SEL _cmd) {
    SWGMCBaseView *gmc;
    
    MusicNowPlayingPlaybackControlsView *control = MSHookIvar<MusicNowPlayingPlaybackControlsView *>(self, "_playbackControlsView");
    
    for (id subview in control.subviews){
        if ([subview isKindOfClass:[SWGMCBaseView class]]){
            gmc = (SWGMCBaseView *)subview;
        }
    }
    
    return gmc;
}



static MPAVController * _logos_method$_ungrouped$MusicNowPlayingViewController$mpavPlayer(MusicNowPlayingViewController* self, SEL _cmd) {
    MusicNowPlayingPlaybackControlsView *control = MSHookIvar<MusicNowPlayingPlaybackControlsView *>(self,
                                                "_playbackControlsView");
    return control.player;
}



static MPAVItem * _logos_method$_ungrouped$MusicNowPlayingViewController$item(MusicNowPlayingViewController* self, SEL _cmd) {
    return MSHookIvar<MPAVItem *>(self, "_item");
}



static unsigned long long _logos_method$_ungrouped$MusicNowPlayingViewController$transportVisibleParts(MusicNowPlayingViewController* self, SEL _cmd) {
    MusicNowPlayingPlaybackControlsView *control = MSHookIvar<MusicNowPlayingPlaybackControlsView *>(self,
                                                                                                     "_playbackControlsView");
    MusicNowPlayingTransportControls *transport = MSHookIvar<MusicNowPlayingTransportControls *>(control,
                                                                                                 "_transportControls");
    return transport.visibleParts;
}

#pragma mark Rating Control 

BOOL didTouchRatingControl = NO;
NSTimer *hideRatingTimer;


static void _logos_method$_ungrouped$MusicNowPlayingViewController$_setShowingRatings$animated$(MusicNowPlayingViewController* self, SEL _cmd, BOOL arg1, BOOL arg2) {
    _logos_orig$_ungrouped$MusicNowPlayingViewController$_setShowingRatings$animated$(self, _cmd, arg1, arg2);
    
    BOOL enabled = [[SWGMCPrefs valueForBaseKey:@"enabled"
                                  forPrefsClass:[SWGMCPrefsMusicAppController class]
                                   defaultValue:@YES] boolValue];
    
    if (!enabled){
        return;
    }
    
    
    
    if (arg1){
        [self startRatingShouldHideTimer];
    } else {
        if (hideRatingTimer){
            [hideRatingTimer invalidate];
            hideRatingTimer = nil;
        }
    }
}



static void _logos_method$_ungrouped$MusicNowPlayingViewController$startRatingShouldHideTimer(MusicNowPlayingViewController* self, SEL _cmd) {
    BOOL isShowingRating = MSHookIvar<BOOL>(self, "_isShowingRatings");
    
    if (hideRatingTimer){
        [hideRatingTimer invalidate];
        hideRatingTimer = nil;
    }
    
    if (!isShowingRating){
        return;
    }
    
    hideRatingTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                       target:self
                                                     selector:@selector(hideRatingControlWithTimer)
                                                     userInfo:nil
                                                      repeats:NO];
}



static void _logos_method$_ungrouped$MusicNowPlayingViewController$hideRatingControlWithTimer(MusicNowPlayingViewController* self, SEL _cmd) {
    BOOL isShowingRating = MSHookIvar<BOOL>(self, "_isShowingRatings");
    
    if (!isShowingRating){
        didTouchRatingControl = NO;
        return;
    }
    
    if (didTouchRatingControl){
        didTouchRatingControl = NO;
        [self startRatingShouldHideTimer];
        return;
    }
    
    [self _setShowingRatings:NO animated:YES];
    didTouchRatingControl = NO;
}








static void _logos_method$_ungrouped$MPURatingControl$_handlePanGesture$(MPURatingControl* self, SEL _cmd, id arg1) {
    _logos_orig$_ungrouped$MPURatingControl$_handlePanGesture$(self, _cmd, arg1);
    didTouchRatingControl = YES;
}


static void _logos_method$_ungrouped$MPURatingControl$_handleTapGesture$(MPURatingControl* self, SEL _cmd, id arg1) {
    _logos_orig$_ungrouped$MPURatingControl$_handleTapGesture$(self, _cmd, arg1);
    didTouchRatingControl = YES;
}






static __attribute__((constructor)) void _logosLocalInit() {
{Class _logos_class$_ungrouped$MusicNowPlayingViewController = objc_getClass("MusicNowPlayingViewController"); MSHookMessageEx(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(viewDidLayoutSubviews), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$viewDidLayoutSubviews, (IMP*)&_logos_orig$_ungrouped$MusicNowPlayingViewController$viewDidLayoutSubviews);MSHookMessageEx(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(viewWillAppear:), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$viewWillAppear$, (IMP*)&_logos_orig$_ungrouped$MusicNowPlayingViewController$viewWillAppear$);MSHookMessageEx(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(viewDidAppear:), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$viewDidAppear$, (IMP*)&_logos_orig$_ungrouped$MusicNowPlayingViewController$viewDidAppear$);MSHookMessageEx(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(willAnimateRotationToInterfaceOrientation:duration:), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$willAnimateRotationToInterfaceOrientation$duration$, (IMP*)&_logos_orig$_ungrouped$MusicNowPlayingViewController$willAnimateRotationToInterfaceOrientation$duration$);MSHookMessageEx(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(didRotateFromInterfaceOrientation:), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$didRotateFromInterfaceOrientation$, (IMP*)&_logos_orig$_ungrouped$MusicNowPlayingViewController$didRotateFromInterfaceOrientation$);{ char _typeEncoding[1024]; unsigned int i = 0; memcpy(_typeEncoding + i, @encode(SWGMCBaseView *), strlen(@encode(SWGMCBaseView *))); i += strlen(@encode(SWGMCBaseView *)); _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(currentGMC), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$currentGMC, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; memcpy(_typeEncoding + i, @encode(MPAVController *), strlen(@encode(MPAVController *))); i += strlen(@encode(MPAVController *)); _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(mpavPlayer), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$mpavPlayer, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; memcpy(_typeEncoding + i, @encode(MPAVItem *), strlen(@encode(MPAVItem *))); i += strlen(@encode(MPAVItem *)); _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(item), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$item, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'Q'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(transportVisibleParts), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$transportVisibleParts, _typeEncoding); }MSHookMessageEx(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(_setShowingRatings:animated:), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$_setShowingRatings$animated$, (IMP*)&_logos_orig$_ungrouped$MusicNowPlayingViewController$_setShowingRatings$animated$);{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(startRatingShouldHideTimer), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$startRatingShouldHideTimer, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$MusicNowPlayingViewController, @selector(hideRatingControlWithTimer), (IMP)&_logos_method$_ungrouped$MusicNowPlayingViewController$hideRatingControlWithTimer, _typeEncoding); }Class _logos_class$_ungrouped$MPURatingControl = objc_getClass("MPURatingControl"); MSHookMessageEx(_logos_class$_ungrouped$MPURatingControl, @selector(_handlePanGesture:), (IMP)&_logos_method$_ungrouped$MPURatingControl$_handlePanGesture$, (IMP*)&_logos_orig$_ungrouped$MPURatingControl$_handlePanGesture$);MSHookMessageEx(_logos_class$_ungrouped$MPURatingControl, @selector(_handleTapGesture:), (IMP)&_logos_method$_ungrouped$MPURatingControl$_handleTapGesture$, (IMP*)&_logos_orig$_ungrouped$MPURatingControl$_handleTapGesture$);} }
#line 305 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCMusicApp.xm"
