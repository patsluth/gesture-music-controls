//
//  SWUIActivityWithAction.m
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-02-26.
//
//

#import "SWUIActivityWithAction.h"

@interface SWUIActivityWithAction()
{
}

@property (strong, nonatomic) swActivityAction currentAction;

@end

@implementation SWUIActivityWithAction

- (id)initWithAction:(swActivityAction)action
{
    self = [super init];
    if (self){
        self.currentAction = action;
    }
    return self;
}

#pragma mark Internal Methods

- (UIImage *)activityImage
{
    return nil;
}

- (void)performActivity
{
    if (self.currentAction){
        self.currentAction();
    }
    
    self.currentAction = nil;
    
    [self activityDidFinish:YES];
}

- (void)dealloc
{
    self.currentAction = nil;
}

@end




