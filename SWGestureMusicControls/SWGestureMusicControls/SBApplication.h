//
//  SBApplication.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@interface SBApplication : NSObject
{
	NSString *_bundleIdentifier;
	NSString *_displayIdentifier;
	NSString *_path;
	NSString *_bundleVersion;
}

@property(copy) NSString *displayIdentifier;

- (id)bundleIdentifier;

@end




