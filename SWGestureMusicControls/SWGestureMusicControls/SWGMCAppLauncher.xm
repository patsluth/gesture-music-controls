//
//  SWGMCAppLauncher.xm
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2/24/2014.
//
//

#import "SWGMCAppLauncher.h"

#import "SBApplication.h"
#import "SBUIController.h"
#import "SBDeviceLockController.h"
#import "SBLockScreenManager.h"
#import "SBLockScreenViewControllerBase.h"
#import "SBUnlockActionContext.h"
#import "SBControlCenterController.h"

@implementation SWGMCAppLauncher : NSObject 

+ (void)doLaunchApp:(SBApplication *)app
{
    if (!app){
        return;
    }
    
	SBUIController *sbUIC = (SBUIController *)[%c(SBUIController) sharedInstanceIfExists];
    
    if (sbUIC){
        [sbUIC activateApplicationAnimated:app];
    }
}

+ (void)launchAppLockScreenFriendly:(SBApplication *)app
{
    if (!app){
		return;
    }
    
    SBDeviceLockController *deviceLC = (SBDeviceLockController *)[%c(SBDeviceLockController) sharedController];
    
	if (deviceLC && deviceLC.isPasscodeLocked){
        
		SBLockScreenManager *manager = (SBLockScreenManager *)[%c(SBLockScreenManager) sharedInstance];
        
		if (manager && manager.isUILocked){
            
			void (^action)() = ^(){
                [SWGMCAppLauncher doLaunchApp:app];
			};
            
			SBLockScreenViewControllerBase *controller = [manager lockScreenViewController];
            
            if (controller){
                
                SBUnlockActionContext *context = [[%c(SBUnlockActionContext) alloc] initWithLockLabel:nil
                                                                                       shortLockLabel:nil
                                                                                         unlockAction:action
                                                                                           identifier:nil];
                context.deactivateAwayController = YES;
                [controller setCustomUnlockActionContext:context];
                
                SBControlCenterController *sbCC = (SBControlCenterController *)[%c(SBControlCenterController)
                                                                                sharedInstanceIfExists];
                if (sbCC){
                    [sbCC dismissAnimated:YES];
                }
                
                [controller setPasscodeLockVisible:YES animated:YES completion:nil];
                
                return;
                
            }
		}
	}
    
    [SWGMCAppLauncher doLaunchApp:app];
}

@end




