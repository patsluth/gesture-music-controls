//
//  SWGMCPrefs.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2/24/2014.
//
//

#import "SWGMCPrefsBundleController.h"
#import "SWGMCPrefsInstanceController.h"

@interface SWGMCPrefs : NSObject
{
}

+ (NSDictionary *)preferences;

+ (id)valueForBaseKey:(NSString *)key forPrefsClass:(Class)prefsClass defaultValue:(id)defaultValue;

+ (NSString *)nullPrimaryTextOverride;
+ (NSString *)nullSecondaryTextOverride;

@end




