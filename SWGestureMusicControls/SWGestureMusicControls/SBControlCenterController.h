//
//  SBControlCenterController.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@interface SBControlCenterController
{
}

+ (id)sharedInstanceIfExists;
+ (id)sharedInstance;
- (void)dismissAnimated:(BOOL)arg1;

@end




