//
//  SWGMCPrefs.xm
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2/24/2014.
//
//

#import "SWGMCPrefs.h"

#define SW_GMC_PREFERENCES_INTERNAL_PATH @"/Library/PreferenceBundles/SWGMCPrefsBundle.bundle/SWGMCPrefsBundle.plist"

static NSDictionary *_preferences;

static NSArray *nowPlayingTextOverrides;

#pragma mark Preferences

@implementation SWGMCPrefs

+ (NSDictionary *)preferences
{
    return _preferences;
}

+ (id)valueForBaseKey:(NSString *)key forPrefsClass:(Class)prefsClass defaultValue:(id)defaultValue
{
    NSString *finalKey = key;
    
    if (prefsClass && [prefsClass isSubclassOfClass:[SWGMCPrefsInstanceController class]]){
        finalKey = [NSString stringWithFormat:@"%@%@", [prefsClass keyPrefix], key];
    }
    
    if (_preferences){
        if (_preferences[finalKey]){
            return _preferences[finalKey];
        }
    }
    
    return defaultValue;
}


#pragma mark Internal Preferences

+ (NSString *)nullPrimaryTextOverride
{
    if (nowPlayingTextOverrides && nowPlayingTextOverrides.count > 0)
        return [nowPlayingTextOverrides objectAtIndex:0];
    
    return @"";
}

+ (NSString *)nullSecondaryTextOverride
{
    if (nowPlayingTextOverrides && nowPlayingTextOverrides.count > 1)
        return [nowPlayingTextOverrides objectAtIndex:1];
    
    return @"";
}

@end

#pragma mark Logos

static void swgcmPreferencesChanged(CFNotificationCenterRef center,
                                        void *observer,
                                        CFStringRef name,
                                        const void *object,
                                        CFDictionaryRef userInfo)
{
    //preferences
    _preferences = [[NSDictionary alloc] initWithContentsOfFile:SW_GMC_PREFERENCES_PATH];
    
    //internal prefs
    NSDictionary *internalPrefs = [NSDictionary
                                   dictionaryWithContentsOfFile:[[NSBundle bundleWithPath:SW_GMC_BUNDLE_PATH]
                                                                 pathForResource:@"SWGMCInternalSettings"
                                                                 ofType:@"plist"]];
    
    nowPlayingTextOverrides = [internalPrefs objectForKey:@"nowplayingtextoverrides"];
}

%ctor
{
    CFNotificationCenterRef darwin = CFNotificationCenterGetDarwinNotifyCenter();
    CFNotificationCenterAddObserver(darwin,
                                    nil,
                                    swgcmPreferencesChanged,
                                    CFSTR(SW_GMC_PREFS_CHANGED_NOTIFICATION),
                                    nil,
                                    CFNotificationSuspensionBehaviorCoalesce);
    // Load preferences
    swgcmPreferencesChanged(nil, nil, nil, nil, nil);
}