//
//  RadioTrack.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@interface RadioTrack : NSObject <NSSecureCoding>
{
}

@property(readonly) NSString *album;
@property(readonly) NSString *artist;
//@property(readonly) RadioArtworkCollection *artworkCollection;
@property(readonly) NSArray *assets;
@property(copy) NSDictionary *metadataDictionary;
@property(readonly) NSString *title;
@property(readonly) NSDictionary *trackDictionary;
@property(readonly) NSDictionary *trackInfo;

@end