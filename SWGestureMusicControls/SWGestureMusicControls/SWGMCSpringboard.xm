//
//  SWGMCControlCenter.xm
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2/24/2014.
//
//

#import "_MPUSystemMediaControlsView.h"
#import "MPUSystemMediaControlsViewController.h"

#import "SWGMCSlideView.h"

#import "SWGMCPrefsLockScreenController.h"
#import "SWGMCPrefsControlCenterController.h"

%hook _MPUSystemMediaControlsView

- (void)layoutSubviews
{
    //fixes play/pause animation
    SWGMCBaseView *gmc = [self currentGMC];
    if (gmc && gmc.shouldBlockLayout){
        return;
    }
    
    %orig();
    
    [self updateGMCFrames];
}

- (void)setFrame:(CGRect)frame
{
    %orig(frame);
    
    [self updateGMCFrames];
}

%new
- (void)updateGMCFrames
{
    if (!self.window.rootViewController){
        return;
    }
    
    Class prefClassType;
    
    if ([self.window.rootViewController isKindOfClass:%c(SBControlCenterController)]){
        prefClassType = [SWGMCPrefsControlCenterController class];
    } else {
        prefClassType = [SWGMCPrefsLockScreenController class];
    }
    
    BOOL enabled = [[SWGMCPrefs valueForBaseKey:@"enabled"
                                  forPrefsClass:prefClassType
                                   defaultValue:@YES] boolValue];
    if (!enabled){
        return;
    }
    
    MPUChronologicalProgressView *progress = MSHookIvar<MPUChronologicalProgressView *>(self, "_timeInformationView");
    MPUMediaControlsTitlesView *title = MSHookIvar<MPUMediaControlsTitlesView *>(self, "_trackInformationView");
    MPUTransportControlsView *transport = MSHookIvar<MPUTransportControlsView *>(self, "_transportControlsView");
    MPUMediaControlsVolumeView *volume = MSHookIvar<MPUMediaControlsVolumeView *>(self, "_volumeView");
    
    if (!progress || !title || !transport || !volume){
        return;
    }
    
    BOOL progressHidden = [[SWGMCPrefs valueForBaseKey:@"hideprogress_enabled"
                                         forPrefsClass:prefClassType
                                          defaultValue:@YES] boolValue];
    BOOL volumeHidden = [[SWGMCPrefs valueForBaseKey:@"hidevolume_enabled"
                                       forPrefsClass:prefClassType
                                        defaultValue:@YES] boolValue];
    
    [volume layoutSubviews];
    
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (!gmc){
        
        gmc = [[SWGMCSlideView alloc] init];
        gmc.prefInstanceClass = prefClassType;
        gmc.mpuSystemMediaControlsView = self;
        
        [self addSubview:gmc];
        title.userInteractionEnabled = NO;
        [gmc.scrollview addSubview:title];
        
        //hide default control buttons
        transport.hidden = YES;
    }
    
    CGRect currentFrame = gmc.frame;
    
    CGFloat gmcHeight = self.frame.size.height - (progressHidden ? 0 : progress.frame.size.height)  - (volumeHidden ? 0 : volume.frame.size.height);
    
    CGRect newFrame = CGRectMake(0,
                                 progressHidden ? 0 : progress.frame.size.height,
                                 self.frame.size.width,
                                 gmcHeight);
    
    if (!CGRectEqualToRect(currentFrame, newFrame)){
        gmc.frame = newFrame;
        [gmc layoutSubviews];
    
        [gmc resetContentOffset];
    }
    
    title.center = CGPointMake(gmc.scrollview.contentSize.width / 2,
                               gmc.scrollview.frame.size.height / 2);
    
    gmc.nowPlayingTitleView = title; //set this after setting center to correctly align action indicator
    [self bringSubviewToFront:gmc];
    [gmc.scrollview bringSubviewToFront:title];
    
    if (progressHidden){
        [progress removeFromSuperview];
    } else {
        [self addSubview:progress];
    }
    
    if (volumeHidden){
        [volume removeFromSuperview];
    } else {
        [self addSubview:volume];
    }
    
    
    progress.frame = CGRectMake(progress.frame.origin.x,
                                0,
                                progress.frame.size.width,
                                progress.frame.size.height);
    volume.frame = CGRectMake(volume.frame.origin.x,
                              self.frame.size.height -volume.frame.size.height,
                              volume.frame.size.width,
                              volume.frame.size.height);
    
    
    [self bringSubviewToFront:progress];
    [self bringSubviewToFront:volume];
    
    //this will refresh the explicit indicator
    [title setTitleText:title.titleText];
}

%new
- (SWGMCBaseView *)currentGMC
{
    SWGMCBaseView *gmc;
    
    for (id subview in self.subviews){
        if ([subview isKindOfClass:[SWGMCBaseView class]]){
            gmc = (SWGMCBaseView *)subview;
        }
    }
    
    return gmc;
}

%new
- (BOOL)showRadioModal
{
    MPUTransportControlsView *transport = MSHookIvar<MPUTransportControlsView *>(self, "_transportControlsView");
    
    if (transport.delegate && [transport.delegate isKindOfClass:%c(MPUSystemMediaControlsViewController)]){
        MPUSystemMediaControlsViewController *vc = (MPUSystemMediaControlsViewController *)transport.delegate;
        [vc _likeBanButtonTapped:nil];
        return YES;
    }
    
    return NO;
}

%end

//Control Center and Lock Screen View Controller
%hook MPUSystemMediaControlsViewController

- (void)viewWillAppear:(BOOL)arg1
{
    %orig(arg1);
    
    _MPUSystemMediaControlsView *mcv =  MSHookIvar<_MPUSystemMediaControlsView *>(self, "_mediaControlsView");
    
    SWGMCBaseView *gmc = [mcv currentGMC];
    if (gmc){
        [gmc resetContentOffset];
    }
}

- (void)viewDidAppear:(BOOL)arg1
{
    %orig(arg1);
    
    _MPUSystemMediaControlsView *mcv =  MSHookIvar<_MPUSystemMediaControlsView *>(self, "_mediaControlsView");
    
    SWGMCBaseView *gmc = [mcv currentGMC];
    if (gmc){
        [gmc resetContentOffset];
    }
}

%end




