//
//  SBLockScreenViewController.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@interface SBLockScreenViewController : SBLockScreenViewControllerBase
{
	//MPUSystemMediaControlsViewController* _mediaControlsViewController;
	//SBLockScreenNowPlayingPluginController* _nowPlayingController;
    
	BOOL _mediaControlsVisible;
}

- (id)lockScreenScrollView;
- (id)lockScreenView;

- (BOOL)isShowingMediaControls;
- (void)_setMediaControlsVisible:(BOOL)visible;
- (void)_toggleMediaControls;

- (void)viewDidDisappear:(BOOL)view;
- (void)viewWillDisappear:(BOOL)view;
- (void)viewWillAppear:(BOOL)view;

@end




