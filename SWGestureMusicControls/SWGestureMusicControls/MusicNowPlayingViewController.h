//
//  MusicNowPlayingViewController.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

#import "MusicNowPlayingTitlesView.h"
#import "MPURatingControl.h"
#import "MusicNowPlayingPlaybackControlsView.h"
#import "MPAVController.h"
#import "MPAVItem.h"

@class SWGMCBaseView;

//music app media controls base view (now playing song view)
@interface MusicNowPlayingViewController : UIViewController
{
    MusicNowPlayingTitlesView *_titlesView;
    MPURatingControl *_ratingControl;
	BOOL _isShowingRatings;
    MusicNowPlayingPlaybackControlsView *_playbackControlsView;
    
    UITapGestureRecognizer *_tapGestureRecognizer;
    
    MPAVItem *_item;
}

- (void)_itemDidChangeNotification:(id)arg1; //arg1 = MusicAVPlayer : MPAVController

- (void)_itemTitlesDidChangeNotification:(id)arg1;
- (void)_playbackContentsDidChangeNotification:(id)arg1;

- (void)willAnimateRotationToInterfaceOrientation:(int)arg1 duration:(double)arg2;
- (void)didRotateFromInterfaceOrientation:(int)arg1;

- (void)_tapAction:(UITapGestureRecognizer *)arg1;
- (void)_setShowingRatings:(BOOL)arg1 animated:(BOOL)arg2;

//new
- (SWGMCBaseView *)currentGMC;
- (MPAVController *)mpavPlayer;
- (MPAVItem *)item;
- (unsigned long long)transportVisibleParts;
- (void)startRatingShouldHideTimer;
- (void)hideRatingControlWithTimer;

@end




