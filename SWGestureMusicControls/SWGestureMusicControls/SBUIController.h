//
//  SBUIController.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@interface SBUIController
{
}

+ (id)sharedInstanceIfExists;
+ (id)sharedInstance;
- (void)activateApplicationAnimatedFromIcon:(id)icon fromLocation:(int)location;
- (void)activateApplicationAnimated:(id)animated;

@end




