//
//  SWGMCMusicRepeatActivity.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-02-26.
//
//

#import "SWUIActivityWithAction.h"

#define SW_GMC_REPEAT_ACTIVITY_TYPE @"com.patsluth.swgesturemusiccontrols.repeatactivity"

//REPEAT MODE DISPLAY VALUES - ACTUAL VALUES as per Music app
//0 - @"Repeat" - Repeat Off
//1 - @"Repeat Song" = Repeat Song
//2 - @"Repeat All" - Repeat All

@interface SWGMCMusicRepeatActivity : SWUIActivityWithAction

@property (nonatomic) int currentRepeatMode;

- (id)initWithAction:(swActivityAction)action andRepeatMode:(int)mode;
+ (NSString *)displayStringForCurrentRepeatMode:(int)mode;
+ (NSString *)displayStringForNextRepeatMode:(int)mode;
+ (UIImage *)imageForNextRepeatMode:(int)mode;

@end




