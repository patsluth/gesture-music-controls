//
//  SWGMCBaseView.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2/24/2014.
//
//

#define DegreesToRadians(x) ((x) * M_PI / 180.0)

#import "SWGMCPrefs.h"
#import "SWGMCPrefsInstanceController.h"

@class SBMediaController;
@class MPAVController;
@class MusicNowPlayingViewController;
@class MPUNowPlayingTitlesView;
@class _MPUSystemMediaControlsView;

typedef enum {
    SW_DIRECTION_NONE,
    SW_DIRECTION_LEFT,
    SW_DIRECTION_RIGHT
} SW_SCROLL_DIRECTION;

//actions
typedef void(^swMediaAction)();

@interface SWGMCBaseView : UIView <UIScrollViewDelegate>

@property (strong, nonatomic) Class prefInstanceClass;

@property (retain, nonatomic) UIScrollView *scrollview;
@property (strong, readonly, nonatomic) UIView *actionDisplayView;
@property (strong, nonatomic) NSMutableArray *actionIndicatorTempViews;

@property (readonly, nonatomic) BOOL shouldBlockLayout;

@property (weak, readonly, nonatomic) SBMediaController *sbMediaController;

@property (weak, nonatomic) UIScrollView *lockscreenScrollview;

@property (strong, nonatomic) MPUNowPlayingTitlesView *nowPlayingTitleView;

//used to invoke Radio star view on Lock Screen and Control Center
@property (weak, nonatomic) _MPUSystemMediaControlsView *mpuSystemMediaControlsView;
//used to check if we are in Music App, and to show Ratings Control
@property (weak, nonatomic) MusicNowPlayingViewController *musicAppNowPlayingVC;

//used to simulate music controls being pressed
@property (weak, nonatomic) UIButton *systemRadioLikeOrBanButton;

@property (readwrite, nonatomic) SW_SCROLL_DIRECTION currentScrollDirection;
@property (readwrite, nonatomic) CGFloat previousScrollOffsetX;

//actions
- (NSNumber *)actionWithBaseKey:(NSString *)baseKey defaultValue:(NSNumber *)defaultValue;
- (swMediaAction)methodForActionWithBaseKey:(NSString *)baseKey defaultValue:(NSNumber *)defaultValue;
- (swMediaAction)methodForAction:(NSNumber *)action;

//init
- (void)resetGestureRecognizers;
- (void)resetContentOffset;

- (BOOL)isAnythingCurrentlyPlaying;
- (BOOL)isInMusicApp;
- (BOOL)isiTunesRadio;

- (CGFloat)getScrollOffsetFromCenter:(UIScrollView *)scrollView;
- (BOOL)shouldUpdateActionIndicator;

- (void)nowPlayingItemTextDidChange;

@end




