//
//  MPAVController.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@class MPMediaItem;
@class MPAVItem;

@interface MPAVController : NSObject
{
    int _playbackMode;
}

+ (id)sharedInstance;

//@property(readonly, nonatomic) MPMediaItem *currentMediaItem; //works with ipod tracks only, not radio
@property(readonly, nonatomic) MPAVItem *currentItem;

- (BOOL)isPlaying;

- (void)togglePlayback;
- (void)play;
- (void)pause;

- (void)setPlaybackIndex:(int)index;
- (void)_changePlaybackItemIndexToIndex:(unsigned)index fromIndex:(unsigned)index2 changeByDelta:(int)delta;
- (void)changePlaybackIndexBy:(long)by;
- (void)changePlaybackIndexBy:(int)by deltaType:(int)type;
- (void)changePlaybackIndexBy:(int)by deltaType:(int)type ignoreElapsedTime:(BOOL)time;

- (double)currentTime;
- (void)setCurrentTime:(double)arg1 options:(int)arg2;
- (void)setCurrentTime:(double)arg1;
- (double)durationOfCurrentItemIfAvailable;

- (BOOL)canSeekBackwards;
- (BOOL)canSeekForwards;

@end




