//
//  SWGMCUIActivityViewController.m
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-03-01.
//
//

#import "SWGMCUIActivityViewController.h"

@interface SWGMCUIActivityViewController ()

@end

@implementation SWGMCUIActivityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){ 
    }
    return self;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end




