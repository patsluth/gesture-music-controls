//
//  _MPUSystemMediaControlsView.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

#import "MPUChronologicalProgressView.h"
#import "MPUMediaControlsTitlesView.h"
#import "MPUTransportControlsView.h"
#import "MPUMediaControlsVolumeView.h"

@class SWGMCBaseView;

//lock screen and control center media controls base view
@interface _MPUSystemMediaControlsView : UIView
{
    MPUChronologicalProgressView *_timeInformationView;
    MPUMediaControlsTitlesView *_trackInformationView;
    MPUTransportControlsView *_transportControlsView;
    MPUMediaControlsVolumeView *_volumeView;
}

- (id)initWithStyle:(int)arg1; //changes color of items, not line numbers

//new
- (void)updateGMCFrames;
- (SWGMCBaseView *)currentGMC;
- (BOOL)showRadioModal;

@end




