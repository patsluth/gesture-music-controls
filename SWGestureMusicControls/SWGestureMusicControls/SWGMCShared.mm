#line 1 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCShared.xm"








#import "SWGMCPrefs.h"
#import "MusicNowPlayingTitlesView.h"
#import "SWGMCBaseView.h"

#import <AppList/ALApplicationList.h>












#include <logos/logos.h>
#include <substrate.h>
@class MPUNowPlayingTitlesView; @class ALApplicationList; @class MusicNowPlayingTitlesView; 
static id (*_logos_orig$_ungrouped$MPUNowPlayingTitlesView$initWithStyle$)(MPUNowPlayingTitlesView*, SEL, int); static id _logos_method$_ungrouped$MPUNowPlayingTitlesView$initWithStyle$(MPUNowPlayingTitlesView*, SEL, int); static void (*_logos_orig$_ungrouped$MPUNowPlayingTitlesView$setTitleText$)(MPUNowPlayingTitlesView*, SEL, NSString *); static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$setTitleText$(MPUNowPlayingTitlesView*, SEL, NSString *); static void (*_logos_orig$_ungrouped$MPUNowPlayingTitlesView$setArtistText$)(MPUNowPlayingTitlesView*, SEL, NSString *); static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$setArtistText$(MPUNowPlayingTitlesView*, SEL, NSString *); static void (*_logos_orig$_ungrouped$MPUNowPlayingTitlesView$setExplicit$)(MPUNowPlayingTitlesView*, SEL, BOOL); static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$setExplicit$(MPUNowPlayingTitlesView*, SEL, BOOL); static void (*_logos_orig$_ungrouped$MPUNowPlayingTitlesView$layoutSubviews)(MPUNowPlayingTitlesView*, SEL); static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$layoutSubviews(MPUNowPlayingTitlesView*, SEL); static void (*_logos_orig$_ungrouped$MPUNowPlayingTitlesView$setExplicitImage$)(MPUNowPlayingTitlesView*, SEL, UIImage *); static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$setExplicitImage$(MPUNowPlayingTitlesView*, SEL, UIImage *); static SWGMCBaseView * _logos_method$_ungrouped$MPUNowPlayingTitlesView$currentGMC(MPUNowPlayingTitlesView*, SEL); static BOOL _logos_method$_ungrouped$MPUNowPlayingTitlesView$textIsNowPlayingAppName$(MPUNowPlayingTitlesView*, SEL, NSString *); 
static __inline__ __attribute__((always_inline)) Class _logos_static_class_lookup$ALApplicationList(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("ALApplicationList"); } return _klass; }static __inline__ __attribute__((always_inline)) Class _logos_static_class_lookup$MusicNowPlayingTitlesView(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("MusicNowPlayingTitlesView"); } return _klass; }
#line 26 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCShared.xm"





static id _logos_method$_ungrouped$MPUNowPlayingTitlesView$initWithStyle$(MPUNowPlayingTitlesView* self, SEL _cmd, int arg1) {
    if (![[SWGMCPrefs valueForBaseKey:@"forcemultilined_enabled"
                      forPrefsClass:nil
                       defaultValue:@YES] boolValue]){
        return _logos_orig$_ungrouped$MPUNowPlayingTitlesView$initWithStyle$(self, _cmd, arg1);
    }
    
    if ([self isKindOfClass:_logos_static_class_lookup$MusicNowPlayingTitlesView()] && ISIPAD){
        return _logos_orig$_ungrouped$MPUNowPlayingTitlesView$initWithStyle$(self, _cmd, arg1);
    }
    
    return _logos_orig$_ungrouped$MPUNowPlayingTitlesView$initWithStyle$(self, _cmd, 1);
}


static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$setTitleText$(MPUNowPlayingTitlesView* self, SEL _cmd, NSString * arg1) {
    NSString *newTextValue = arg1;
    
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (gmc){
        if (newTextValue){
            if ([self textIsNowPlayingAppName:newTextValue]){
                newTextValue = [SWGMCPrefs nullPrimaryTextOverride];
            }
        } else {
            newTextValue = [SWGMCPrefs nullPrimaryTextOverride];
        }
    }
    
    if ([newTextValue isEqualToString:[SWGMCPrefs nullPrimaryTextOverride]]){
        [self setExplicit:NO];
    }
    
    _logos_orig$_ungrouped$MPUNowPlayingTitlesView$setTitleText$(self, _cmd, newTextValue);
    [self setArtistText:self.artistText]; 
    
    
    if (gmc){
        [gmc nowPlayingItemTextDidChange];
    }
    
    
    [self setExplicit:[self isExplicit]];
}



static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$setArtistText$(MPUNowPlayingTitlesView* self, SEL _cmd, NSString * arg1) {
    NSString *newTextValue = arg1;
    
    if ([self.superview.superview isKindOfClass:[SWGMCBaseView class]]){
        if (!newTextValue && [self.titleText isEqualToString:[SWGMCPrefs nullPrimaryTextOverride]]){
            newTextValue = [SWGMCPrefs nullSecondaryTextOverride];
        }
    }
    
    _logos_orig$_ungrouped$MPUNowPlayingTitlesView$setArtistText$(self, _cmd, newTextValue);
}


static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$setExplicit$(MPUNowPlayingTitlesView* self, SEL _cmd, BOOL arg1) {
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (gmc){
        
        if ([[SWGMCPrefs valueForBaseKey:@"forceexplicit_enabled"
                           forPrefsClass:gmc.prefInstanceClass
                            defaultValue:@YES] boolValue]){ 
            if (self.titleText &&
                ![self.titleText isEqualToString:@""] &&
                ![self.titleText isEqualToString:[SWGMCPrefs nullPrimaryTextOverride]]){
                _logos_orig$_ungrouped$MPUNowPlayingTitlesView$setExplicit$(self, _cmd, YES);
                return;
            }
        }
    }
    
    _logos_orig$_ungrouped$MPUNowPlayingTitlesView$setExplicit$(self, _cmd, arg1);
}


static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$layoutSubviews(MPUNowPlayingTitlesView* self, SEL _cmd) {
    _logos_orig$_ungrouped$MPUNowPlayingTitlesView$layoutSubviews(self, _cmd);
    
    
    UIImageView *explicitIV =  MSHookIvar<UIImageView *>(self, "_explicitImageView");
    if (explicitIV){
        explicitIV.tintColor = [self _detailLabel].textColor;
    }
}


static void _logos_method$_ungrouped$MPUNowPlayingTitlesView$setExplicitImage$(MPUNowPlayingTitlesView* self, SEL _cmd, UIImage * arg1) {
    
    _logos_orig$_ungrouped$MPUNowPlayingTitlesView$setExplicitImage$(self, _cmd, [arg1 imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]);
}



static SWGMCBaseView * _logos_method$_ungrouped$MPUNowPlayingTitlesView$currentGMC(MPUNowPlayingTitlesView* self, SEL _cmd) {
    SWGMCBaseView *gmc;
    
    
    
    if ([self.superview.superview isKindOfClass:[SWGMCBaseView class]]){
        gmc = (SWGMCBaseView *)self.superview.superview;
    }
    
    return gmc;
}



static BOOL _logos_method$_ungrouped$MPUNowPlayingTitlesView$textIsNowPlayingAppName$(MPUNowPlayingTitlesView* self, SEL _cmd, NSString * text) {
    NSDictionary *appList = [_logos_static_class_lookup$ALApplicationList() sharedApplicationList].applications;
    
    if (appList){
        for (NSString *appName in appList){
            if ([text isEqualToString:[appList valueForKey:appName]]){
                return YES;
            }
        }
    }
    
    return NO;
}






static __attribute__((constructor)) void _logosLocalInit() {
{Class _logos_class$_ungrouped$MPUNowPlayingTitlesView = objc_getClass("MPUNowPlayingTitlesView"); MSHookMessageEx(_logos_class$_ungrouped$MPUNowPlayingTitlesView, @selector(initWithStyle:), (IMP)&_logos_method$_ungrouped$MPUNowPlayingTitlesView$initWithStyle$, (IMP*)&_logos_orig$_ungrouped$MPUNowPlayingTitlesView$initWithStyle$);MSHookMessageEx(_logos_class$_ungrouped$MPUNowPlayingTitlesView, @selector(setTitleText:), (IMP)&_logos_method$_ungrouped$MPUNowPlayingTitlesView$setTitleText$, (IMP*)&_logos_orig$_ungrouped$MPUNowPlayingTitlesView$setTitleText$);MSHookMessageEx(_logos_class$_ungrouped$MPUNowPlayingTitlesView, @selector(setArtistText:), (IMP)&_logos_method$_ungrouped$MPUNowPlayingTitlesView$setArtistText$, (IMP*)&_logos_orig$_ungrouped$MPUNowPlayingTitlesView$setArtistText$);MSHookMessageEx(_logos_class$_ungrouped$MPUNowPlayingTitlesView, @selector(setExplicit:), (IMP)&_logos_method$_ungrouped$MPUNowPlayingTitlesView$setExplicit$, (IMP*)&_logos_orig$_ungrouped$MPUNowPlayingTitlesView$setExplicit$);MSHookMessageEx(_logos_class$_ungrouped$MPUNowPlayingTitlesView, @selector(layoutSubviews), (IMP)&_logos_method$_ungrouped$MPUNowPlayingTitlesView$layoutSubviews, (IMP*)&_logos_orig$_ungrouped$MPUNowPlayingTitlesView$layoutSubviews);MSHookMessageEx(_logos_class$_ungrouped$MPUNowPlayingTitlesView, @selector(setExplicitImage:), (IMP)&_logos_method$_ungrouped$MPUNowPlayingTitlesView$setExplicitImage$, (IMP*)&_logos_orig$_ungrouped$MPUNowPlayingTitlesView$setExplicitImage$);{ char _typeEncoding[1024]; unsigned int i = 0; memcpy(_typeEncoding + i, @encode(SWGMCBaseView *), strlen(@encode(SWGMCBaseView *))); i += strlen(@encode(SWGMCBaseView *)); _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$MPUNowPlayingTitlesView, @selector(currentGMC), (IMP)&_logos_method$_ungrouped$MPUNowPlayingTitlesView$currentGMC, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; memcpy(_typeEncoding + i, @encode(BOOL), strlen(@encode(BOOL))); i += strlen(@encode(BOOL)); _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; memcpy(_typeEncoding + i, @encode(NSString *), strlen(@encode(NSString *))); i += strlen(@encode(NSString *)); _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$MPUNowPlayingTitlesView, @selector(textIsNowPlayingAppName:), (IMP)&_logos_method$_ungrouped$MPUNowPlayingTitlesView$textIsNowPlayingAppName$, _typeEncoding); }} }
#line 164 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCShared.xm"
