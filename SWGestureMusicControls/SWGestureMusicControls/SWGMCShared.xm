//
//  SWGMCShared.xm
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2/27/2014.
//
//

#import "SWGMCPrefs.h"
#import "MusicNowPlayingTitlesView.h"
#import "SWGMCBaseView.h"

#import <AppList/ALApplicationList.h>

/*%hook MPUMediaControlsTitlesView

//1 - ControlCenter default (white primary text, black secondary text)
//2 - LockScreen default (white primary text, grey secondary text)
- (id)initWithMediaControlsStyle:(int)arg1
{
    return %orig(arg1);
}

%end*/

%hook MPUNowPlayingTitlesView

//1 - 2 lines
//2 - 1 line
- (id)initWithStyle:(int)arg1
{
    if (![[SWGMCPrefs valueForBaseKey:@"forcemultilined_enabled"
                      forPrefsClass:nil
                       defaultValue:@YES] boolValue]){
        return %orig(arg1);
    }
    
    if ([self isKindOfClass:%c(MusicNowPlayingTitlesView)] && ISIPAD){
        return %orig(arg1);
    }
    
    return %orig(1);
}

- (void)setTitleText:(NSString *)arg1
{
    NSString *newTextValue = arg1;
    
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (gmc){
        if (newTextValue){
            if ([self textIsNowPlayingAppName:newTextValue]){
                newTextValue = [SWGMCPrefs nullPrimaryTextOverride];
            }
        } else {
            newTextValue = [SWGMCPrefs nullPrimaryTextOverride];
        }
    }
    
    if ([newTextValue isEqualToString:[SWGMCPrefs nullPrimaryTextOverride]]){
        [self setExplicit:NO];
    }
    
    %orig(newTextValue);
    [self setArtistText:self.artistText]; //refresh
    
    //make sure we dont scroll around before we update the text
    if (gmc){
        [gmc nowPlayingItemTextDidChange];
    }
    
    //refresh
    [self setExplicit:[self isExplicit]];
}

//only set artist text to our custom text if we know there is nothing playing
- (void)setArtistText:(NSString *)arg1
{
    NSString *newTextValue = arg1;
    
    if ([self.superview.superview isKindOfClass:[SWGMCBaseView class]]){
        if (!newTextValue && [self.titleText isEqualToString:[SWGMCPrefs nullPrimaryTextOverride]]){
            newTextValue = [SWGMCPrefs nullSecondaryTextOverride];
        }
    }
    
    %orig(newTextValue);
}

- (void)setExplicit:(BOOL)arg1
{
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (gmc){
        
        if ([[SWGMCPrefs valueForBaseKey:@"forceexplicit_enabled"
                           forPrefsClass:gmc.prefInstanceClass
                            defaultValue:@YES] boolValue]){ //IF EXPLICIT IS ENABLED FOR GMC INSTANCE
            if (self.titleText &&
                ![self.titleText isEqualToString:@""] &&
                ![self.titleText isEqualToString:[SWGMCPrefs nullPrimaryTextOverride]]){
                %orig(YES);
                return;
            }
        }
    }
    
    %orig(arg1);
}

- (void)layoutSubviews
{
    %orig();
    
    //fix for blurred music app showing incorrect Explicit image color. BITCH
    UIImageView *explicitIV =  MSHookIvar<UIImageView *>(self, "_explicitImageView");
    if (explicitIV){
        explicitIV.tintColor = [self _detailLabel].textColor;
    }
}

- (void)setExplicitImage:(UIImage *)arg1
{
    //fix for blurred music app showing incorrect Explicit image color. BITCH
    %orig([arg1 imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]);
}

%new
- (SWGMCBaseView *)currentGMC
{
    SWGMCBaseView *gmc;
    
    //self.superview == our scroll view
    //our scroll views superview is SWGMCBaseView
    if ([self.superview.superview isKindOfClass:[SWGMCBaseView class]]){
        gmc = (SWGMCBaseView *)self.superview.superview;
    }
    
    return gmc;
}

%new
- (BOOL)textIsNowPlayingAppName:(NSString *)text
{
    NSDictionary *appList = [%c(ALApplicationList) sharedApplicationList].applications;
    
    if (appList){
        for (NSString *appName in appList){
            if ([text isEqualToString:[appList valueForKey:appName]]){
                return YES;
            }
        }
    }
    
    return NO;
}

%end




