//
//  SWGMCAppLauncher.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@class SBApplication;

@interface SWGMCAppLauncher : NSObject
{
}

+ (void)launchAppLockScreenFriendly:(SBApplication *)app;

@end




