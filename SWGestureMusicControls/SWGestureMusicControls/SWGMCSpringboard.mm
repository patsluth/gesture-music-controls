#line 1 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCSpringboard.xm"








#import "_MPUSystemMediaControlsView.h"
#import "MPUSystemMediaControlsViewController.h"

#import "SWGMCSlideView.h"

#import "SWGMCPrefsLockScreenController.h"
#import "SWGMCPrefsControlCenterController.h"

#include <logos/logos.h>
#include <substrate.h>
@class MPUSystemMediaControlsViewController; @class SBControlCenterController; @class _MPUSystemMediaControlsView; 
static void (*_logos_orig$_ungrouped$_MPUSystemMediaControlsView$layoutSubviews)(_MPUSystemMediaControlsView*, SEL); static void _logos_method$_ungrouped$_MPUSystemMediaControlsView$layoutSubviews(_MPUSystemMediaControlsView*, SEL); static void (*_logos_orig$_ungrouped$_MPUSystemMediaControlsView$setFrame$)(_MPUSystemMediaControlsView*, SEL, CGRect); static void _logos_method$_ungrouped$_MPUSystemMediaControlsView$setFrame$(_MPUSystemMediaControlsView*, SEL, CGRect); static void _logos_method$_ungrouped$_MPUSystemMediaControlsView$updateGMCFrames(_MPUSystemMediaControlsView*, SEL); static SWGMCBaseView * _logos_method$_ungrouped$_MPUSystemMediaControlsView$currentGMC(_MPUSystemMediaControlsView*, SEL); static BOOL _logos_method$_ungrouped$_MPUSystemMediaControlsView$showRadioModal(_MPUSystemMediaControlsView*, SEL); static void (*_logos_orig$_ungrouped$MPUSystemMediaControlsViewController$viewWillAppear$)(MPUSystemMediaControlsViewController*, SEL, BOOL); static void _logos_method$_ungrouped$MPUSystemMediaControlsViewController$viewWillAppear$(MPUSystemMediaControlsViewController*, SEL, BOOL); static void (*_logos_orig$_ungrouped$MPUSystemMediaControlsViewController$viewDidAppear$)(MPUSystemMediaControlsViewController*, SEL, BOOL); static void _logos_method$_ungrouped$MPUSystemMediaControlsViewController$viewDidAppear$(MPUSystemMediaControlsViewController*, SEL, BOOL); 
static __inline__ __attribute__((always_inline)) Class _logos_static_class_lookup$MPUSystemMediaControlsViewController(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("MPUSystemMediaControlsViewController"); } return _klass; }static __inline__ __attribute__((always_inline)) Class _logos_static_class_lookup$SBControlCenterController(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("SBControlCenterController"); } return _klass; }
#line 17 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCSpringboard.xm"



static void _logos_method$_ungrouped$_MPUSystemMediaControlsView$layoutSubviews(_MPUSystemMediaControlsView* self, SEL _cmd) {
    
    SWGMCBaseView *gmc = [self currentGMC];
    if (gmc && gmc.shouldBlockLayout){
        return;
    }
    
    _logos_orig$_ungrouped$_MPUSystemMediaControlsView$layoutSubviews(self, _cmd);
    
    [self updateGMCFrames];
}


static void _logos_method$_ungrouped$_MPUSystemMediaControlsView$setFrame$(_MPUSystemMediaControlsView* self, SEL _cmd, CGRect frame) {
    _logos_orig$_ungrouped$_MPUSystemMediaControlsView$setFrame$(self, _cmd, frame);
    
    [self updateGMCFrames];
}



static void _logos_method$_ungrouped$_MPUSystemMediaControlsView$updateGMCFrames(_MPUSystemMediaControlsView* self, SEL _cmd) {
    if (!self.window.rootViewController){
        return;
    }
    
    Class prefClassType;
    
    if ([self.window.rootViewController isKindOfClass:_logos_static_class_lookup$SBControlCenterController()]){
        prefClassType = [SWGMCPrefsControlCenterController class];
    } else {
        prefClassType = [SWGMCPrefsLockScreenController class];
    }
    
    BOOL enabled = [[SWGMCPrefs valueForBaseKey:@"enabled"
                                  forPrefsClass:prefClassType
                                   defaultValue:@YES] boolValue];
    if (!enabled){
        return;
    }
    
    MPUChronologicalProgressView *progress = MSHookIvar<MPUChronologicalProgressView *>(self, "_timeInformationView");
    MPUMediaControlsTitlesView *title = MSHookIvar<MPUMediaControlsTitlesView *>(self, "_trackInformationView");
    MPUTransportControlsView *transport = MSHookIvar<MPUTransportControlsView *>(self, "_transportControlsView");
    MPUMediaControlsVolumeView *volume = MSHookIvar<MPUMediaControlsVolumeView *>(self, "_volumeView");
    
    if (!progress || !title || !transport || !volume){
        return;
    }
    
    BOOL progressHidden = [[SWGMCPrefs valueForBaseKey:@"hideprogress_enabled"
                                         forPrefsClass:prefClassType
                                          defaultValue:@YES] boolValue];
    BOOL volumeHidden = [[SWGMCPrefs valueForBaseKey:@"hidevolume_enabled"
                                       forPrefsClass:prefClassType
                                        defaultValue:@YES] boolValue];
    
    [volume layoutSubviews];
    
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (!gmc){
        
        gmc = [[SWGMCSlideView alloc] init];
        gmc.prefInstanceClass = prefClassType;
        gmc.mpuSystemMediaControlsView = self;
        
        [self addSubview:gmc];
        title.userInteractionEnabled = NO;
        [gmc.scrollview addSubview:title];
        
        
        transport.hidden = YES;
    }
    
    CGRect currentFrame = gmc.frame;
    
    CGFloat gmcHeight = self.frame.size.height - (progressHidden ? 0 : progress.frame.size.height)  - (volumeHidden ? 0 : volume.frame.size.height);
    
    CGRect newFrame = CGRectMake(0,
                                 progressHidden ? 0 : progress.frame.size.height,
                                 self.frame.size.width,
                                 gmcHeight);
    
    if (!CGRectEqualToRect(currentFrame, newFrame)){
        gmc.frame = newFrame;
        [gmc layoutSubviews];
    
        [gmc resetContentOffset];
    }
    
    title.center = CGPointMake(gmc.scrollview.contentSize.width / 2,
                               gmc.scrollview.frame.size.height / 2);
    
    gmc.nowPlayingTitleView = title; 
    [self bringSubviewToFront:gmc];
    [gmc.scrollview bringSubviewToFront:title];
    
    if (progressHidden){
        [progress removeFromSuperview];
    } else {
        [self addSubview:progress];
    }
    
    if (volumeHidden){
        [volume removeFromSuperview];
    } else {
        [self addSubview:volume];
    }
    
    
    progress.frame = CGRectMake(progress.frame.origin.x,
                                0,
                                progress.frame.size.width,
                                progress.frame.size.height);
    volume.frame = CGRectMake(volume.frame.origin.x,
                              self.frame.size.height -volume.frame.size.height,
                              volume.frame.size.width,
                              volume.frame.size.height);
    
    
    [self bringSubviewToFront:progress];
    [self bringSubviewToFront:volume];
    
    
    [title setTitleText:title.titleText];
}



static SWGMCBaseView * _logos_method$_ungrouped$_MPUSystemMediaControlsView$currentGMC(_MPUSystemMediaControlsView* self, SEL _cmd) {
    SWGMCBaseView *gmc;
    
    for (id subview in self.subviews){
        if ([subview isKindOfClass:[SWGMCBaseView class]]){
            gmc = (SWGMCBaseView *)subview;
        }
    }
    
    return gmc;
}



static BOOL _logos_method$_ungrouped$_MPUSystemMediaControlsView$showRadioModal(_MPUSystemMediaControlsView* self, SEL _cmd) {
    MPUTransportControlsView *transport = MSHookIvar<MPUTransportControlsView *>(self, "_transportControlsView");
    
    if (transport.delegate && [transport.delegate isKindOfClass:_logos_static_class_lookup$MPUSystemMediaControlsViewController()]){
        MPUSystemMediaControlsViewController *vc = (MPUSystemMediaControlsViewController *)transport.delegate;
        [vc _likeBanButtonTapped:nil];
        return YES;
    }
    
    return NO;
}







static void _logos_method$_ungrouped$MPUSystemMediaControlsViewController$viewWillAppear$(MPUSystemMediaControlsViewController* self, SEL _cmd, BOOL arg1) {
    _logos_orig$_ungrouped$MPUSystemMediaControlsViewController$viewWillAppear$(self, _cmd, arg1);
    
    _MPUSystemMediaControlsView *mcv =  MSHookIvar<_MPUSystemMediaControlsView *>(self, "_mediaControlsView");
    
    SWGMCBaseView *gmc = [mcv currentGMC];
    if (gmc){
        [gmc resetContentOffset];
    }
}


static void _logos_method$_ungrouped$MPUSystemMediaControlsViewController$viewDidAppear$(MPUSystemMediaControlsViewController* self, SEL _cmd, BOOL arg1) {
    _logos_orig$_ungrouped$MPUSystemMediaControlsViewController$viewDidAppear$(self, _cmd, arg1);
    
    _MPUSystemMediaControlsView *mcv =  MSHookIvar<_MPUSystemMediaControlsView *>(self, "_mediaControlsView");
    
    SWGMCBaseView *gmc = [mcv currentGMC];
    if (gmc){
        [gmc resetContentOffset];
    }
}






static __attribute__((constructor)) void _logosLocalInit() {
{Class _logos_class$_ungrouped$_MPUSystemMediaControlsView = objc_getClass("_MPUSystemMediaControlsView"); MSHookMessageEx(_logos_class$_ungrouped$_MPUSystemMediaControlsView, @selector(layoutSubviews), (IMP)&_logos_method$_ungrouped$_MPUSystemMediaControlsView$layoutSubviews, (IMP*)&_logos_orig$_ungrouped$_MPUSystemMediaControlsView$layoutSubviews);MSHookMessageEx(_logos_class$_ungrouped$_MPUSystemMediaControlsView, @selector(setFrame:), (IMP)&_logos_method$_ungrouped$_MPUSystemMediaControlsView$setFrame$, (IMP*)&_logos_orig$_ungrouped$_MPUSystemMediaControlsView$setFrame$);{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$_MPUSystemMediaControlsView, @selector(updateGMCFrames), (IMP)&_logos_method$_ungrouped$_MPUSystemMediaControlsView$updateGMCFrames, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; memcpy(_typeEncoding + i, @encode(SWGMCBaseView *), strlen(@encode(SWGMCBaseView *))); i += strlen(@encode(SWGMCBaseView *)); _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$_MPUSystemMediaControlsView, @selector(currentGMC), (IMP)&_logos_method$_ungrouped$_MPUSystemMediaControlsView$currentGMC, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; memcpy(_typeEncoding + i, @encode(BOOL), strlen(@encode(BOOL))); i += strlen(@encode(BOOL)); _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$_MPUSystemMediaControlsView, @selector(showRadioModal), (IMP)&_logos_method$_ungrouped$_MPUSystemMediaControlsView$showRadioModal, _typeEncoding); }Class _logos_class$_ungrouped$MPUSystemMediaControlsViewController = objc_getClass("MPUSystemMediaControlsViewController"); MSHookMessageEx(_logos_class$_ungrouped$MPUSystemMediaControlsViewController, @selector(viewWillAppear:), (IMP)&_logos_method$_ungrouped$MPUSystemMediaControlsViewController$viewWillAppear$, (IMP*)&_logos_orig$_ungrouped$MPUSystemMediaControlsViewController$viewWillAppear$);MSHookMessageEx(_logos_class$_ungrouped$MPUSystemMediaControlsViewController, @selector(viewDidAppear:), (IMP)&_logos_method$_ungrouped$MPUSystemMediaControlsViewController$viewDidAppear$, (IMP*)&_logos_orig$_ungrouped$MPUSystemMediaControlsViewController$viewDidAppear$);} }
#line 210 "/Users/patsluth/Programming/iOS/gesture-music-controls/SWGestureMusicControls/SWGestureMusicControls/SWGMCSpringboard.xm"
