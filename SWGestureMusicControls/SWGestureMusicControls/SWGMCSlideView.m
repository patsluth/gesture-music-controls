//
//  SWGMCSlideView.m
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2/24/2014.
//
//

#import "SWGMCSlideView.h"
#import "SWGMCPrefs.h"

#import "SBMediaController.h"

#import "MusicNowPlayingViewController.h"

@interface SWGMCSlideView()
{
}

@property (readwrite, nonatomic) CGFloat currentVelocity;
@property (strong, nonatomic) NSTimer *wrapAroundFallback;

@end

@implementation SWGMCSlideView

- (id)init
{
    self = [super init];
    
    if (self){
        self.scrollview.decelerationRate = UIScrollViewDecelerationRateFast;
        [self resetContentOffset];
        
        self.scrollview.canCancelContentTouches = YES;
    }
    
    return self;
}

- (void)resetContentOffset
{
    self.scrollview.contentOffset = CGPointMake(self.scrollview.frame.size.width, 0.0);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGPoint originalContentOffset = self.scrollview.contentOffset;
    
    self.scrollview.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width * 3,
                                             self.scrollview.frame.size.height);
    
    self.scrollview.contentOffset = originalContentOffset;
}

#pragma mark ScrollView
/*
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
    
    if ([self shouldUpdateActionIndicator]){
        CGFloat xOffsetFromCenter = fabs([self getScrollOffsetFromCenter:scrollView]);
        
        //dont start fading until we have scrolled 25% of the width of the scroll view
        xOffsetFromCenter -= (self.scrollview.frame.size.width * 0.2);
        
        CGFloat actionIndicatorAlpha = xOffsetFromCenter / (scrollView.frame.size.width * 0.35);
        actionIndicatorAlpha = 0.0;
        self.songSkipIndicator.alpha = actionIndicatorAlpha;
        
    } else {
        self.songSkipIndicator.alpha = 0.0;
    }
}
*/
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset
{
    self.currentVelocity = velocity.x;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [super scrollViewDidEndDecelerating:scrollView];
    
    CGFloat pageWidth = self.scrollview.frame.size.width;
    float fractionalPage = self.scrollview.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    BOOL shouldAnimated = (page != 1);
    
    if (shouldAnimated){
        
        self.scrollview.userInteractionEnabled = NO;
        long trackDirection = (page == 0) ? 1 : -1;
        
        NSNumber *actionValue;
        
        if (trackDirection == -1){
            actionValue = [self actionWithBaseKey:@"swipeLeftAction" defaultValue:@2];
        } else {
            actionValue = [self actionWithBaseKey:@"swipeRightAction" defaultValue:@3];
        }
        
        swMediaAction action = [self methodForAction:actionValue];
        
        if (action){
            
            action();
            
            //only check for song change if we have a song change action. DUH
            if ([actionValue isEqualToNumber:@2] || [actionValue isEqualToNumber:@3]){
                if (![self isInMusicApp]){
                    
                    if (self.sbMediaController.nowPlayingTitle && ![self.sbMediaController.nowPlayingTitle isEqualToString:@""]){
                        [self startWrapAroundFallback];
                    } else {
                        [self finishWrapAroundAnimation];
                    }
                } else {
                    //seems to be time for deciding whether to go to the previous song
                    //or just restart the current song
                    //trial and error
                    if ((trackDirection == -1 && [self.musicAppNowPlayingVC
                                                  mpavPlayer].currentTime > 3.0) || [self isiTunesRadio]){
                        [self finishWrapAroundAnimation];
                    } else {
                        [self startWrapAroundFallback];
                    }
                }
            } else {
                [self finishWrapAroundAnimation];
            }
        } else {
            [self finishWrapAroundAnimation];
        }
    } else {
        self.scrollview.contentOffset = CGPointMake(self.scrollview.frame.size.width, 0);
    }
}

- (void)finishWrapAroundAnimation
{
    [self stopWrapAroundFallback];
    
    self.scrollview.userInteractionEnabled = YES;
    
    CGFloat pageWidth = self.scrollview.frame.size.width;
    float fractionalPage = self.scrollview.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    CGPoint targetContentOffset = CGPointMake(self.scrollview.frame.size.width, 0);
    
    if (page == 0){
        self.scrollview.contentOffset = CGPointMake(self.scrollview.frame.size.width * 2, 0);
    } else if (page == 2){
        self.scrollview.contentOffset = CGPointZero;
    }
    
    if (self.currentVelocity == 0.0){
        self.currentVelocity = self.scrollview.decelerationRate;
    }
    
    //calcualte distance we need to animate
    CGFloat xDistance = fabs(self.scrollview.contentOffset.x - targetContentOffset.x);
    //get total animation time using the points/ms we got from
    //scrollViewWillEndDragging with velocity (converted to seconds)
    CGFloat animationTime = (xDistance / fabs(self.currentVelocity)) / 1000;
    
    //reset
    self.currentVelocity = 0.0;
    
    [UIView animateWithDuration:animationTime
                          delay:0.0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.scrollview.contentOffset = targetContentOffset;
                     }completion:^(BOOL finished){
                         
                     }];
}

- (CGFloat)getScrollOffsetFromCenter:(UIScrollView *)scrollView
{
    return scrollView.contentOffset.x - scrollView.frame.size.width;
}

#pragma mark Public Methods

- (void)nowPlayingItemTextDidChange
{
    [super nowPlayingItemTextDidChange];
    
    CGFloat pageWidth = self.scrollview.frame.size.width;
    float fractionalPage = self.scrollview.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    if ((page == 0 || page == 2)){
        [self finishWrapAroundAnimation];
    }
}

- (void)startWrapAroundFallback
{
    [self stopWrapAroundFallback];
    
    self.wrapAroundFallback = [NSTimer scheduledTimerWithTimeInterval:1.5
                                                               target:self
                                                             selector:@selector(finishWrapAroundAnimation)
                                                             userInfo:nil
                                                              repeats:NO];
}

- (void)stopWrapAroundFallback
{
    if (self.wrapAroundFallback){
        [self.wrapAroundFallback invalidate];
        self.wrapAroundFallback = nil;
    }
}

- (void)dealloc
{
    [self stopWrapAroundFallback];
}

@end




