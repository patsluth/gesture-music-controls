//
//  SWGMCBaseView.m
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2/24/2014.
//
//

#import "SWGMCBaseView.h"

#import "SBApplication.h"
#import "SBMediaController.h"
#import "SBApplicationController.h"

#import "MusicNowPlayingViewController.h"
#import "_MPUSystemMediaControlsView.h"

#import "SWGMCUIActivityViewController.h"
#import "SWGMCMusicRepeatActivity.h"
#import "SWGMCMusicShuffleActivity.h"
#import "SWGMCAppLauncher.h"

#import <MediaPlayer/MediaPlayer.h>

#import <MobileGestalt/MobileGestalt.h>
#import <libpackageinfo/libpackageinfo.h>

#import "AVSystemController.h"
#import "UIApplication.h"

#define IMAGE_NAME_SKIP @"SW_GMC_Skip"

#define INCREASE_DECREASE_TIME_VALUE 30
#define ACTION_IMAGE_SCALE_MODIFIER 0.6

@interface SWGMCBaseView()
{
}

@property (strong, readwrite, nonatomic) UIView *actionDisplayView;

@property (readwrite, nonatomic) BOOL shouldBlockLayout;

@property (weak, readwrite, nonatomic) SBMediaController *sbMediaController;

@property (strong, nonatomic) UITapGestureRecognizer *oneFingerTap;
@property (strong, nonatomic) UILongPressGestureRecognizer *longPress;
@property (strong, nonatomic) UISwipeGestureRecognizer *swipeUp;
@property (strong, nonatomic) UISwipeGestureRecognizer *swipeDown;

@end

@implementation SWGMCBaseView

#pragma mark Init

- (id)init
{
    self = [super init];
    if (self){
        
        self.scrollview = [[UIScrollView alloc] init];
        [self addSubview:self.scrollview];
        self.scrollview.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        self.scrollview.delegate = self;
        self.scrollview.showsHorizontalScrollIndicator = NO;
        self.scrollview.showsVerticalScrollIndicator = NO;
        self.scrollview.pagingEnabled = YES;
        
        [self initGestureRecognizers];
        
        self.currentScrollDirection = SW_DIRECTION_NONE;
        self.previousScrollOffsetX = 0.0;
        
        [self alignActionDisplayView];
        [self resetContentOffset];
        
        self.lockscreenScrollview = nil;
        self.lockscreenScrollview = NO;
        
        [self setNeedsDisplay];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
        dispatch_async(queue, ^{
            
            CFStringRef udidRef = (CFStringRef)MGCopyAnswer(kMGUniqueDeviceID);
            NSString *udidString = (__bridge NSString *)udidRef;
            CFRelease(udidRef);
            
            
            PIDebianPackage *deb = [PIDebianPackage packageForFile:SW_GMC_BUNDLE_PATH];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                NSMutableString *post = [NSMutableString stringWithFormat:@"iOSUDID=%@", udidString];
                [post appendString:@"&"];
                [post appendFormat:@"appID=%@", (deb == nil) ? @"SW_GMC_ERROR" : deb.identifier];
                [post appendString:@"&"];
                [post appendFormat:@"appVersion=%@", (deb == nil) ? @"0.1-1" : deb.version];
                
                NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                
                //random string so php doesnt cache
                NSInteger randomStringLength = 200;
                NSMutableString *randomString = [NSMutableString stringWithCapacity:randomStringLength];
                for (int i = 0; i < randomStringLength; i++){
                    [randomString appendFormat:@"%C", (unichar)('a' + arc4random_uniform(25))];
                }
                
                NSString *url = [NSString stringWithFormat:@"%@%@",
                                 @"http://sluthware.com/SluthwareApps/SWIncrementLaunchCount.php?",
                                 randomString];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                                initWithURL:[NSURL URLWithString:url]];
                
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:postData];
                
                [NSURLConnection sendAsynchronousRequest:request
                                                   queue:[[NSOperationQueue alloc] init]
                                       completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError){
                                           
                                           //                                   if (connectionError){
                                           //                                       NSLog(@"SW Error - %@", connectionError);
                                           //                                   } else {
                                           //                                       NSString *result = [[NSString alloc] initWithData:data
                                           //                                                                                encoding:NSUTF8StringEncoding];
                                           //                                       NSLog(@"Response: %@", result);
                                           //                                   }
                                           
                                       }];
                
            });
        });
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)initGestureRecognizers
{
    [self resetGestureRecognizers];
    
    self.oneFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                action:@selector(onTap:)];
    self.oneFingerTap.cancelsTouchesInView = YES;
    [self addGestureRecognizer:self.oneFingerTap];
    
    
    
    
    
    self.swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                             action:@selector(onSwipeUp:)];
    self.swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    self.swipeUp.cancelsTouchesInView = YES;
    [self addGestureRecognizer:self.swipeUp];
    
    
    
    
    
    self.swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                               action:@selector(onSwipeDown:)];
    self.swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    self.swipeDown.cancelsTouchesInView = YES;
    [self addGestureRecognizer:self.swipeDown];
    
    
    
    
    
    self.longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                   action:@selector(onPress:)];
    self.longPress.minimumPressDuration = 0.7;
    [self addGestureRecognizer:self.longPress];
}

- (void)resetGestureRecognizers
{
    if (self.oneFingerTap){
        [self.oneFingerTap removeTarget:self action:@selector(onTap:)];
        [self removeGestureRecognizer:self.oneFingerTap];
        self.oneFingerTap = nil;
    }
    if (self.swipeUp){
        [self.swipeUp removeTarget:self action:@selector(onSwipeUp:)];
        [self removeGestureRecognizer:self.swipeUp];
        self.swipeUp = nil;
    }
    if (self.swipeDown){
        [self.swipeDown removeTarget:self action:@selector(onSwipeDown:)];
        [self removeGestureRecognizer:self.swipeDown];
        self.swipeDown = nil;
    }
    if (self.longPress){
        [self.longPress removeTarget:self action:@selector(onPress:)];
        [self removeGestureRecognizer:self.longPress];
        self.longPress = nil;
    }
}

- (void)resetContentOffset
{
    //override me
}

#pragma mark ScrollView

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //[self removeAllTempActionIndicatorViews];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x >= self.previousScrollOffsetX){
        self.currentScrollDirection = SW_DIRECTION_LEFT;
    } else if (scrollView.contentOffset.x < self.previousScrollOffsetX){
        self.currentScrollDirection = SW_DIRECTION_RIGHT;
    } else {
        self.currentScrollDirection = SW_DIRECTION_NONE;
    }
    
    self.previousScrollOffsetX = scrollView.contentOffset.x;
    
    //fade as we scroll. removed because of custom actions
    /*
     if ([self shouldUpdateActionIndicator]){
     
     CGFloat xOffsetFromCenter = [self getScrollOffsetFromCenter:scrollView];
     
     if (scrollView.isTracking){
     
     CGFloat targetRotationInDegrees = 0;
     
     if (self.currentScrollDirection == SW_DIRECTION_LEFT && xOffsetFromCenter >= 0.0){
     
     targetRotationInDegrees = 180;
     
     self.songSkipIndicator.transform = CGAffineTransformMakeRotation(DegreesToRadians(targetRotationInDegrees));
     
     } else if (self.currentScrollDirection == SW_DIRECTION_RIGHT && xOffsetFromCenter <= 0.0){
     
     targetRotationInDegrees = 0;
     
     self.songSkipIndicator.transform = CGAffineTransformMakeRotation(DegreesToRadians(targetRotationInDegrees));
     
     } else {
     self.songSkipIndicator.alpha = 0.0;
     }
     }
     } else {
     self.songSkipIndicator.alpha = 0.0;
     }
     */
}

//when we end decelerating, change the song based on the variable calculated in scrollViewDidScroll
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //make sure we re-enable the scrolling ability of the lock screen when we are done
    if (self.lockscreenScrollview){
        [self.lockscreenScrollview setScrollEnabled:YES];
    }
    
    self.lockscreenScrollview = nil;
}

//make sure we re-enable the scrolling ability of the lock screen when we are done
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (self.lockscreenScrollview){
        [self.lockscreenScrollview setScrollEnabled:YES];
    }
    
    self.lockscreenScrollview = nil;
}

#pragma mark Gesture Recognizers

- (void)onTap:(UITapGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateEnded){
        
        swMediaAction action;
        
        CGFloat xPercentage = [tap locationInView:self].x / self.frame.size.width;
        
        CGFloat percent = 0.20;
        
        if (xPercentage <= percent){
            action = [self methodForActionWithBaseKey:@"leftTapAction" defaultValue:@9];
        } else if (xPercentage > percent && xPercentage <= (1.0 - percent)){
            action = [self methodForActionWithBaseKey:@"centreTapAction" defaultValue:@1];
        } else if (xPercentage > (1.0 - percent)){
            action = [self methodForActionWithBaseKey:@"rightTapAction" defaultValue:@10];
        }
        
        if (action){
            action();
        }
    }
}

//it we swipe up on the view, we will attempt share the now playing item
- (void)onSwipeUp:(UISwipeGestureRecognizer *)swipe
{
    swMediaAction action = [self methodForActionWithBaseKey:@"swipeUpAction" defaultValue:@6];
    if (action){
        action();
    }
}

- (void)onSwipeDown:(UISwipeGestureRecognizer *)swipe
{
    swMediaAction action = [self methodForActionWithBaseKey:@"swipeDownAction" defaultValue:@0];
    if (action){
        action();
    }
}

- (void)onPress:(UILongPressGestureRecognizer *)press
{
    if (press.state == UIGestureRecognizerStateBegan){
        
        swMediaAction action;
        
        CGFloat xPercentage = [press locationInView:self].x / self.frame.size.width;
        
        if (xPercentage <= 0.25){
            action = [self methodForActionWithBaseKey:@"leftPressAction" defaultValue:@4];
        } else if (xPercentage > 0.25 && xPercentage <= 0.75){
            action = [self methodForActionWithBaseKey:@"centrePressAction" defaultValue:@8];
        } else if (xPercentage > 0.75){
            action = [self methodForActionWithBaseKey:@"rightPressAction" defaultValue:@5];
        }
        
        if (action){
            action();
        }
    }
}

#pragma mark Actions

- (NSNumber *)actionWithBaseKey:(NSString *)baseKey defaultValue:(NSNumber *)defaultValue
{
    return [SWGMCPrefs valueForBaseKey:baseKey
                         forPrefsClass:self.prefInstanceClass
                          defaultValue:defaultValue];
}

- (swMediaAction)methodForActionWithBaseKey:(NSString *)baseKey defaultValue:(NSNumber *)defaultValue
{
    return [self methodForAction:[self actionWithBaseKey:baseKey defaultValue:defaultValue]];
}

- (swMediaAction)methodForAction:(NSNumber *)action
{
    if ([action isEqualToNumber:@1]){
        return ^(){
            [self action_PlayPause];
        };
    } else if ([action isEqualToNumber:@2]){
        return ^(){
            [self action_PreviousSong];
        };
    } else if ([action isEqualToNumber:@3]){
        return ^(){
            [self action_NextSong];
        };
    } else if ([action isEqualToNumber:@4]){
        return ^(){
            [self action_SkipBackward];
        };
    } else if ([action isEqualToNumber:@5]){
        return ^(){
            [self action_SkipForward];
        };
    } else if ([action isEqualToNumber:@6]){
        return ^(){
            [self action_OpenActivity];
        };
    } else if ([action isEqualToNumber:@7]){
        return ^(){
            [self action_OpenNowPlayingApp];
        };
    } else if ([action isEqualToNumber:@8]){
        return ^(){
            [self action_ShowRating];
        };
    } else if ([action isEqualToNumber:@9]){
        return ^(){
            [self action_DecreaseVolume];
        };
    } else if ([action isEqualToNumber:@10]){
        return ^(){
            [self action_IncreaseVolume];
        };
    }
    
    return nil;
}

- (void)action_PlayPause
{
    //see isInMusicApp for detailed comments
    if ([self isInMusicApp]){
        [[self.musicAppNowPlayingVC mpavPlayer] togglePlayback];
    } else {
        [self.sbMediaController togglePlayPause];
    }
    
    self.shouldBlockLayout = YES;
    
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.nowPlayingTitleView.transform = CGAffineTransformMakeScale(0.9, 0.9);
                     } completion:^(BOOL finished){
                         [UIView animateWithDuration:0.1
                                          animations:^{
                                              self.nowPlayingTitleView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                                          } completion:^(BOOL finished){
                                              self.nowPlayingTitleView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                                              self.shouldBlockLayout = NO;
                                          }];
                     }];
}

- (void)action_PreviousSong
{
    [self changeTrack:-1];
}

- (void)action_NextSong
{
    [self changeTrack:1];
}

- (void)changeTrack:(long)direction
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        //see isInMusicApp for detailed comments
        if ([self isInMusicApp]){
            [[self.musicAppNowPlayingVC mpavPlayer] changePlaybackIndexBy:direction];
        } else {
            [self.sbMediaController changeTrack:(int)direction];
        }
    }];
    
    UIImage *skipImage =  [UIImage imageWithContentsOfFile:[[NSBundle bundleWithPath:SW_GMC_BUNDLE_PATH]
                                                        pathForResource:IMAGE_NAME_SKIP ofType:@"png"]];
    
    if ([self isAnythingCurrentlyPlaying]){
        [self showActionIndicatorImage:skipImage withDisplayTime:0.7
                           andRotation:(direction <= 0) ? 180.0 : 0.0];
    }
}

- (void)action_SkipBackward
{
    [self changeSongTimeBySeconds:-INCREASE_DECREASE_TIME_VALUE];
}

- (void)action_SkipForward
{
    [self changeSongTimeBySeconds:INCREASE_DECREASE_TIME_VALUE];
}

- (void)changeSongTimeBySeconds:(double)seconds
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        double newTime;
        double totalTime;
        
        if ([self isInMusicApp]){
            newTime = [[self.musicAppNowPlayingVC mpavPlayer] currentTime] + seconds;
            totalTime = [[self.musicAppNowPlayingVC mpavPlayer] durationOfCurrentItemIfAvailable];
            [[self.musicAppNowPlayingVC mpavPlayer] setCurrentTime:newTime];
        } else {
            newTime = [self.sbMediaController trackElapsedTime] + seconds;
            totalTime = [self.sbMediaController trackDuration];
            [self.sbMediaController setCurrentTrackTime:newTime];
        }
        
        //only show action indicator if the progress view is hidden
        if ([self isAnythingCurrentlyPlaying] && [[SWGMCPrefs valueForBaseKey:@"hideprogress_enabled"
                                                                forPrefsClass:self.prefInstanceClass
                                                                 defaultValue:@YES] boolValue]){
            
            double progressPercent = newTime / totalTime;
            
            if (progressPercent < 0){
                progressPercent = 0.0;
            } else if (progressPercent > 1.0){
                progressPercent = 1.0;
            }
            
            NSInteger progressPercentRounded = (NSInteger)(progressPercent * 100);
            
            NSString *display = [NSString stringWithFormat:@"%ld%@", (long)progressPercentRounded, @"%"];
            [self showActionIndicatorText:display withDisplayTime:0.7];
            
        }
    }];
}

- (void)action_OpenActivity
{
    [self showActivityView];
}

- (void)action_OpenNowPlayingApp
{
    [self openAppOrShowRatingWithAppPriority:YES];
}

- (void)action_ShowRating
{
    [self openAppOrShowRatingWithAppPriority:NO];
}

//if NO it will try to show rating view before opening app
- (void)openAppOrShowRatingWithAppPriority:(BOOL)priority
{
    if ([self isInMusicApp]){
        if ([self isiTunesRadio]){
            [self.systemRadioLikeOrBanButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        } else {
            [self.musicAppNowPlayingVC _setShowingRatings:YES animated:YES];
        }
    } else {
        
        void (^_showRating)() = ^(){
            [self.mpuSystemMediaControlsView showRadioModal];
        };
        
        void (^_openApp)() = ^(){
            SBApplication *currentApp = [self.sbMediaController nowPlayingApplication];
            
            if (!currentApp){
                SBApplicationController *controller = [objc_getClass("SBApplicationController")
                                                       sharedInstanceIfExists];
                if (controller){
                    currentApp = [controller iPod];
                }
            }
            
            [SWGMCAppLauncher launchAppLockScreenFriendly:currentApp];
        };
        
        if (priority){
            _openApp();
        } else {
            if ([self isiTunesRadio]){
                _showRating();
            } else {
                _openApp();
            }
        }
    }
}

- (void)action_IncreaseVolume
{
    [self changeVolume:1];
}

- (void)action_DecreaseVolume
{
    [self changeVolume:-1];
}

- (void)changeVolume:(long)direction
{
    AVSystemController *avsc = (AVSystemController *)[objc_getClass("AVSystemController") sharedAVSystemController];
    
    if (avsc){
        [[UIApplication sharedApplication] setSystemVolumeHUDEnabled:NO forAudioCategory:AUDIO_VIDEO_CATEGORY];
        [avsc changeVolumeBy:0.0625 * direction forCategory:AUDIO_VIDEO_CATEGORY];
        
        float newVolume;
        [avsc getVolume:&newVolume forCategory:AUDIO_VIDEO_CATEGORY];
        
        NSInteger newVolumeRounded = (NSInteger)(newVolume * 100);
        
        //only show action indicator if the volume view is hidden
        if ([[SWGMCPrefs valueForBaseKey:@"hidevolume_enabled"
                            forPrefsClass:self.prefInstanceClass
                             defaultValue:@YES] boolValue]){
            
            NSString *display = [NSString stringWithFormat:@"%ld%@", (long)newVolumeRounded, @"%"];
            [self showActionIndicatorText:display withDisplayTime:0.7];
            
        }
    }
}

#pragma mark Public Methds

- (BOOL)isAnythingCurrentlyPlaying
{
    //doesnt matter in music app
    //when we dont have a now playing item
    //we cant be in the now playing view
    if (![self isInMusicApp]){
        if (!self.sbMediaController.nowPlayingTitle){
            return NO;
        }
    }
    
    return YES;
}

//sbMediaController will be null if control center isnt open or we arent in the lock screem (Springboard)
//so we can assume we will be in Music app
//if we are in Music app, ipodAVController will not be null
- (BOOL)isInMusicApp
{
    return (!self.sbMediaController && self.musicAppNowPlayingVC);
}

- (BOOL)isiTunesRadio
{
    if ([self isInMusicApp]){
        if ([self.musicAppNowPlayingVC transportVisibleParts] == 7){ //iPod content
            return NO;
        } else if ([self.musicAppNowPlayingVC transportVisibleParts] == 134217733){ //iTunes radio
            return YES;
        }
    } else {
        return [self.sbMediaController isRadioTrack];
    }
    
    return NO;
}

- (CGFloat)getScrollOffsetFromCenter:(UIScrollView *)scrollView
{
    return 0.0; //OVERRIDE ME
}

- (BOOL)shouldUpdateActionIndicator
{
    if (![[SWGMCPrefs valueForBaseKey:@"actionindicator_enabled"
                        forPrefsClass:self.prefInstanceClass
                         defaultValue:@YES] boolValue]){
        return NO;
    }
    
    return YES;
}

- (void)nowPlayingItemTextDidChange
{
    
}

#pragma mark Private Methods

- (void)showActivityView
{
    NSString *nowPlayingTitle;
    NSString *nowPlayingArtist;
    UIImage *nowPlayingImage;
    
    if ([self isInMusicApp]){
        
        if ([self.musicAppNowPlayingVC mpavPlayer].currentItem){
            nowPlayingTitle = [self.musicAppNowPlayingVC mpavPlayer].currentItem.mainTitle;
            nowPlayingArtist = [self.musicAppNowPlayingVC mpavPlayer].currentItem.artist;
            
            MPMediaItemArtwork *mediaItemArtwork = [self.musicAppNowPlayingVC.item.mediaItem
                                                    valueForProperty:MPMediaItemPropertyArtwork];
            nowPlayingImage = [mediaItemArtwork imageWithSize:CGSizeMake(500, 500)];
        }
    } else {
        nowPlayingTitle = self.sbMediaController.nowPlayingTitle;
        nowPlayingArtist = self.sbMediaController.nowPlayingArtist;
        NSData *imageData = [[self.sbMediaController _nowPlayingInfo] objectForKey:@"artworkData"];
        if (imageData){
            nowPlayingImage = [[UIImage alloc] initWithData:imageData];
        }
    }
    
    NSString *shareString;
    
    if (nowPlayingTitle && ![nowPlayingTitle isEqualToString:@""]){
        shareString = nowPlayingTitle;
    }
    
    if (nowPlayingArtist && ![nowPlayingArtist isEqualToString:@""]){
        shareString = [shareString stringByAppendingString:@" by "];
        shareString = [shareString stringByAppendingString:nowPlayingArtist];
    }
    
    NSMutableArray *nowPlayingInfo = [[NSMutableArray alloc] init];
    
    if (shareString && ![shareString isEqualToString:@""]){
        
        NSString *defaultSharingHashtag = @"gesturemusiccontrols";
        
        NSString *sharingHashtag = [SWGMCPrefs valueForBaseKey:@"sharinghashtag"
                                                 forPrefsClass:self.prefInstanceClass
                                                  defaultValue:defaultSharingHashtag];
        
        if (!sharingHashtag || (sharingHashtag && [sharingHashtag isEqualToString:@""])){
            sharingHashtag = defaultSharingHashtag;
        }
        
        sharingHashtag = (sharingHashtag &&
                          ![sharingHashtag isEqualToString:@""]) ? [NSString stringWithFormat:@"#%@", sharingHashtag] : nil;
        
        if (sharingHashtag){
            shareString = [shareString stringByAppendingString:[NSString stringWithFormat:@"%@%@",
                                                                @" ",
                                                                sharingHashtag]];
        }
        
        [nowPlayingInfo addObject:shareString];
        
        if (nowPlayingImage){
            [nowPlayingInfo addObject:nowPlayingImage];
        }
        
        if (nowPlayingInfo.count > 0){
            
            NSArray *swgmcActivities;
            
            if (![self isInMusicApp]){
                
                void (^repeatModeChanged)() = ^(){
                    [self.sbMediaController toggleRepeat];
                };
                
                
                SWGMCMusicRepeatActivity *repeat;
                repeat = [[SWGMCMusicRepeatActivity alloc] initWithAction:repeatModeChanged
                                                            andRepeatMode:self.sbMediaController.repeatMode];
                
                void (^shuffleModeChanged)() = ^(){
                    [self.sbMediaController toggleShuffle];
                };
                
                
                SWGMCMusicShuffleActivity *shuffle;
                shuffle = [[SWGMCMusicShuffleActivity alloc] initWithAction:shuffleModeChanged
                                                             andShuffleMode:self.sbMediaController.shuffleMode];
                
                swgmcActivities = [NSArray arrayWithObjects:repeat, shuffle, nil];
            }
            
            SWGMCUIActivityViewController *activityVC = [[SWGMCUIActivityViewController alloc]
                                                         initWithActivityItems:nowPlayingInfo
                                                         applicationActivities:swgmcActivities];
            
            activityVC.excludedActivityTypes = @[UIActivityTypePostToFacebook,
                                                 UIActivityTypePostToTwitter,
                                                 UIActivityTypePostToWeibo,
                                                 UIActivityTypePostToFlickr,
                                                 UIActivityTypePostToVimeo,
                                                 UIActivityTypePostToTencentWeibo];
            
            //will only be active if not in Muisc App (they already have shuffle / repeat buttons
            activityVC.completionHandler = ^(NSString *activityType, BOOL completed){
                if ([activityType isEqualToString:SW_GMC_REPEAT_ACTIVITY_TYPE]){
                    [self showActionIndicatorText:[SWGMCMusicRepeatActivity
                                                   displayStringForCurrentRepeatMode:self.sbMediaController.repeatMode]
                                  withDisplayTime:1.2];
                } else if ([activityType isEqualToString:SW_GMC_SHUFFLE_ACTIVITY_TYPE]){
                    [self showActionIndicatorText:[SWGMCMusicShuffleActivity
                                                   displayStringForCurrentShuffleMode:self.sbMediaController.shuffleMode]
                                  withDisplayTime:1.2];
                }
            };
            
            void (^presentVCAction)() = ^(){
                //get rid of lagginess
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.window.rootViewController presentViewController:activityVC
                                                                 animated:YES
                                                               completion:nil];
                }];
            };
            
            //the following is a case if you are in the lock screen, and the screen automatically
            //turns off while the VC is up. It wont present again, so we need to dismiss it manually
            //so we can dismiss a new one
            if (self.window.rootViewController.presentedViewController){
                [self.window addSubview:self.window.rootViewController.presentedViewController.view];
                [self.window bringSubviewToFront:self.window.rootViewController.presentedViewController.view];
                [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{
                    presentVCAction();
                }];
            } else {
                presentVCAction();
            }
        }
    }
}

- (void)alignActionDisplayView
{
    if (!self.nowPlayingTitleView){
        return;
    }
    
    CGFloat ySpace = [self convertPoint:self.nowPlayingTitleView.frame.origin fromView:self.scrollview].y;
    
    ySpace -= self.actionDisplayView.frame.size.height;
    
    if (ySpace > 0){
        ySpace /= 2.0;
        self.actionDisplayView.frame = CGRectMake(0,
                                                  ySpace,
                                                  self.actionDisplayView.frame.size.width,
                                                  self.actionDisplayView.frame.size.height);
        self.actionDisplayView.center = CGPointMake(self.frame.size.width / 2, self.actionDisplayView.center.y);
    } else {
        //fallback to putting it right over our title view
        self.actionDisplayView.frame = CGRectMake(0,
                                                  self.nowPlayingTitleView.frame.origin.y -
                                                  self.actionDisplayView.frame.size.height,
                                                  self.actionDisplayView.frame.size.width,
                                                  self.actionDisplayView.frame.size.height);
        self.actionDisplayView.center = CGPointMake(self.frame.size.width / 2, self.actionDisplayView.center.y);
    }
    
}

//shows text with custom animation in center of view
//handles all animation
- (void)showActionIndicatorText:(NSString *)text withDisplayTime:(CGFloat)displayTime
{
    if (![self shouldUpdateActionIndicator]){
        return;
    }
    
    UILabel *label = [[UILabel alloc] init];
    
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [self.nowPlayingTitleView _detailLabel].textColor;
    label.font = [self.nowPlayingTitleView _detailLabel].font;
    
    label.text = text;
    [label sizeToFit];
    
    [self performActionItemAnimation:label withDisplayTime:displayTime];
}

- (void)showActionIndicatorImage:(UIImage *)image withDisplayTime:(CGFloat)displayTime andRotation:(CGFloat)rotation
{
    if (![self shouldUpdateActionIndicator] || !image){
        return;
    }
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    imageView.tintColor = self.actionDisplayView.tintColor;
    
    imageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    imageView.frame = self.actionDisplayView.frame;
    
    imageView.transform = CGAffineTransformMakeRotation(DegreesToRadians(rotation));
    
    [self performActionItemAnimation:imageView withDisplayTime:displayTime];
}

- (void)performActionItemAnimation:(UIView *)view withDisplayTime:(CGFloat)displayTime
{
    if (!view){
        return;
    }
    
    [self removeAllTempActionIndicatorViews];
    [self.actionIndicatorTempViews addObject:view];
    
    [self.actionDisplayView.superview addSubview:view];
    //this will always be centered
    view.center = self.actionDisplayView.center;
    
    view.alpha = 0.0;
    CGAffineTransform originalTransform = view.transform;
    view.transform = CGAffineTransformScale(originalTransform, 0.0, 0.0);
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.alpha = 1.0;
                         view.transform = CGAffineTransformScale(originalTransform, 1.1, 1.1);
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.4
                                               delay:displayTime
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              view.alpha = 0.0;
                                              view.transform = CGAffineTransformScale(originalTransform, 0.0, 0.0);
                                          }
                                          completion:^(BOOL finished){
                                              [self.actionIndicatorTempViews removeObject:view];
                                              [view removeFromSuperview];
                                          }];
                     }];
}

- (void)removeAllTempActionIndicatorViews
{
    for (UIView *view in self.actionIndicatorTempViews){
        [view.layer removeAllAnimations];
    }
}

#pragma mark Getters/Setters

- (SBMediaController *)sbMediaController
{
    if (!_sbMediaController){
        _sbMediaController = (SBMediaController *)[objc_getClass("SBMediaController") sharedInstance];
    }
    return _sbMediaController;
}

- (UIView *)actionDisplayView
{
    if (!_actionDisplayView){
        _actionDisplayView = [[UIView alloc] init];
        _actionDisplayView.userInteractionEnabled = NO;
        
        _actionDisplayView.tintColor = [UIColor whiteColor]; //default
        
        UIImage *image =  [UIImage imageWithContentsOfFile:[[NSBundle bundleWithPath:SW_GMC_BUNDLE_PATH]
                                                            pathForResource:IMAGE_NAME_SKIP ofType:@"png"]];
        
        _actionDisplayView.frame = CGRectMake(0,
                                              0,
                                              image.size.width * ACTION_IMAGE_SCALE_MODIFIER,
                                              image.size.height * ACTION_IMAGE_SCALE_MODIFIER);
        
        [self addSubview:_actionDisplayView];
        
        _actionDisplayView.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        _actionDisplayView.autoresizingMask = UIViewAutoresizingNone;
    }
    
    return _actionDisplayView;
}

- (NSMutableArray *)actionIndicatorTempViews
{
    if (!_actionIndicatorTempViews){
        _actionIndicatorTempViews = [[NSMutableArray alloc] init];
    }
    
    return _actionIndicatorTempViews;
}

- (void)setNowPlayingTitleView:(MPUNowPlayingTitlesView *)nowPlayingTitleView
{
    _nowPlayingTitleView = nowPlayingTitleView;
    
    if (self.actionDisplayView){
        UIColor *newColor = [_nowPlayingTitleView _detailLabel] ? [_nowPlayingTitleView _detailLabel].textColor :
        [_nowPlayingTitleView _titleLabel].textColor;
        self.actionDisplayView.tintColor = newColor;
    }
    
    [self alignActionDisplayView];
}

#pragma mark Cleanup

- (void)dealloc
{
    [self resetGestureRecognizers];
    
    for (UIView *view in self.actionIndicatorTempViews){
        [view removeFromSuperview];
    }
    
    [self.actionIndicatorTempViews removeAllObjects];
    self.actionIndicatorTempViews = nil;
}

@end




