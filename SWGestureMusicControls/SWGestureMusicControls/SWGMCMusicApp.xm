//
//  SWGMCMusicApp.xm
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2/24/2014.
//
//

#import "SWGMCSlideView.h"
#import "SWGMCPrefsMusicAppController.h"

#import "MusicNowPlayingViewController.h"
#import "MusicNowPlayingTitlesView.h"
#import "MusicNowPlayingPlaybackControlsView.h"
#import "MPDetailSlider.h"
#import "MusicNowPlayingTransportControls.h"
#import "MPURatingControl.h"
#import "MPVolumeSlider.h"
#import "MPAVController.h"
#import "MPButton.h"
#import "MPAVItem.h"

#pragma mark MusicNowPlayingViewController

%hook MusicNowPlayingViewController

- (void)viewDidLayoutSubviews
{
    //fixes play/pause animation
    SWGMCBaseView *gmc = [self currentGMC];
    if (gmc && gmc.shouldBlockLayout){
        return;
    }
    
    %orig();
    
    BOOL enabled = [[SWGMCPrefs valueForBaseKey:@"enabled"
                                  forPrefsClass:[SWGMCPrefsMusicAppController class]
                                   defaultValue:@YES] boolValue];
    
    if (!enabled){
        return;
    }
    
    
    MusicNowPlayingTitlesView *title =  MSHookIvar<MusicNowPlayingTitlesView *>(self, "_titlesView");
    MusicNowPlayingPlaybackControlsView *control = MSHookIvar<MusicNowPlayingPlaybackControlsView *>(self,
                                                                   "_playbackControlsView");
    MPDetailSlider *progressControl =  MSHookIvar<MPDetailSlider *>(control, "_progressControl");
    
    MusicNowPlayingTransportControls *transport = MSHookIvar<MusicNowPlayingTransportControls *>(control,
                                                                "_transportControls");
    
    MPURatingControl *rating = MSHookIvar<MPURatingControl *>(self, "_ratingControl");
    MPVolumeSlider *volume = MSHookIvar<MPVolumeSlider *>(control, "_volumeSlider");
    
    BOOL progressHidden = [[SWGMCPrefs valueForBaseKey:@"hideprogress_enabled"
                                         forPrefsClass:[SWGMCPrefsMusicAppController class]
                                          defaultValue:@YES] boolValue];
    BOOL volumeHidden = [[SWGMCPrefs valueForBaseKey:@"hidevolume_enabled"
                                       forPrefsClass:[SWGMCPrefsMusicAppController class]
                                        defaultValue:@YES] boolValue];
    
    //make sure all controls have the correct frames before adding our view
    [control layoutSubviews];
    //hide default control buttons
    transport.hidden = YES;
    
    if (!gmc){
        
        gmc = [[SWGMCSlideView alloc] init];
        gmc.prefInstanceClass = [SWGMCPrefsMusicAppController class];
        //update controller so we can still control playback
        gmc.musicAppNowPlayingVC = self;
        
        [control addSubview:gmc];
        [gmc.scrollview addSubview:title];

        gmc.systemRadioLikeOrBanButton = MSHookIvar<MPButton *>(transport, "_likeOrBanButton");
    }
    
    CGRect currentFrame = gmc.frame;
    
    CGFloat gmcHeight = volume.frame.origin.y - progressControl.frame.origin.y - (progressHidden ? 0 : progressControl.frame.size.height) + (volumeHidden ? volume.frame.size.height : 0);
    
    CGRect newFrame = CGRectMake(self.view.frame.origin.x,
                                 progressControl.frame.origin.y + (progressHidden ? 0 : progressControl.frame.size.height),
                                 self.view.frame.size.width,
                                 gmcHeight);
    
    if (!CGRectEqualToRect(currentFrame, newFrame)){
        gmc.frame = newFrame;
        [gmc layoutSubviews];
        
        [gmc resetContentOffset];
    }
    
    //make sure we are centered to account for extra space left by transport controls being removed
    title.frame = CGRectMake(0,
                             (gmc.scrollview.frame.size.height / 2) - (title.frame.size.height / 2),
                             title.frame.size.width,
                             title.frame.size.height);
    title.center = CGPointMake(gmc.scrollview.contentSize.width / 2, title.center.y);
    
    gmc.nowPlayingTitleView = title; //set this after setting center to correctly align action indicator
    
    //convert the center from the music control scroll view to the main view, so we can correctly center the rating
    CGPoint convertedCenter = [gmc.scrollview convertPoint:title.center toView:rating.superview];
    rating.center = convertedCenter;
    
    [control bringSubviewToFront:gmc];
    [gmc.scrollview bringSubviewToFront:title];
    
    [transport.superview bringSubviewToFront:transport];
    [progressControl.superview bringSubviewToFront:progressControl];
    [volume.superview bringSubviewToFront:volume];
    
    if (progressHidden){
        [progressControl removeFromSuperview];
    } else {
        if (!progressControl.superview){
            [gmc.superview addSubview:progressControl];
        }
    }
    
    if (volumeHidden){
        [volume removeFromSuperview];
    } else {
        if (!volume.superview){
            [gmc.superview addSubview:volume];
        }
    }
    
    //this will refresh the explicit indicator
    [title setTitleText:title.titleText];
}

- (void)viewWillAppear:(BOOL)arg1
{
    %orig(arg1);
    
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (gmc){
        [gmc resetContentOffset];
    }
}

- (void)viewDidAppear:(BOOL)arg1
{
    %orig(arg1);
    
    SWGMCBaseView *gmc = [self currentGMC];
    
    if (gmc){
        [gmc resetContentOffset];
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(int)arg1 duration:(double)arg2
{
    %orig(arg1, arg2);
    [self viewDidLayoutSubviews];
}

- (void)didRotateFromInterfaceOrientation:(int)arg1
{
    %orig(arg1);
    [self viewDidLayoutSubviews];
}

%new
- (SWGMCBaseView *)currentGMC
{
    SWGMCBaseView *gmc;
    
    MusicNowPlayingPlaybackControlsView *control = MSHookIvar<MusicNowPlayingPlaybackControlsView *>(self, "_playbackControlsView");
    
    for (id subview in control.subviews){
        if ([subview isKindOfClass:[SWGMCBaseView class]]){
            gmc = (SWGMCBaseView *)subview;
        }
    }
    
    return gmc;
}

%new
- (MPAVController *)mpavPlayer
{
    MusicNowPlayingPlaybackControlsView *control = MSHookIvar<MusicNowPlayingPlaybackControlsView *>(self,
                                                "_playbackControlsView");
    return control.player;
}

%new
- (MPAVItem *)item
{
    return MSHookIvar<MPAVItem *>(self, "_item");
}

%new
- (unsigned long long)transportVisibleParts
{
    MusicNowPlayingPlaybackControlsView *control = MSHookIvar<MusicNowPlayingPlaybackControlsView *>(self,
                                                                                                     "_playbackControlsView");
    MusicNowPlayingTransportControls *transport = MSHookIvar<MusicNowPlayingTransportControls *>(control,
                                                                                                 "_transportControls");
    return transport.visibleParts;
}

#pragma mark Rating Control 

BOOL didTouchRatingControl = NO;
NSTimer *hideRatingTimer;

- (void)_setShowingRatings:(BOOL)arg1 animated:(BOOL)arg2
{
    %orig(arg1, arg2);
    
    BOOL enabled = [[SWGMCPrefs valueForBaseKey:@"enabled"
                                  forPrefsClass:[SWGMCPrefsMusicAppController class]
                                   defaultValue:@YES] boolValue];
    
    if (!enabled){
        return;
    }
    
    //fix for broken BlurredMusicApp on first chosen song
    //lol not sure why lol cakes
    if (arg1){
        [self startRatingShouldHideTimer];
    } else {
        if (hideRatingTimer){
            [hideRatingTimer invalidate];
            hideRatingTimer = nil;
        }
    }
}

%new
- (void)startRatingShouldHideTimer
{
    BOOL isShowingRating = MSHookIvar<BOOL>(self, "_isShowingRatings");
    
    if (hideRatingTimer){
        [hideRatingTimer invalidate];
        hideRatingTimer = nil;
    }
    
    if (!isShowingRating){
        return;
    }
    
    hideRatingTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                       target:self
                                                     selector:@selector(hideRatingControlWithTimer)
                                                     userInfo:nil
                                                      repeats:NO];
}

%new
- (void)hideRatingControlWithTimer
{
    BOOL isShowingRating = MSHookIvar<BOOL>(self, "_isShowingRatings");
    
    if (!isShowingRating){
        didTouchRatingControl = NO;
        return;
    }
    
    if (didTouchRatingControl){
        didTouchRatingControl = NO;
        [self startRatingShouldHideTimer];
        return;
    }
    
    [self _setShowingRatings:NO animated:YES];
    didTouchRatingControl = NO;
}

%end

%hook MPURatingControl

//so we know if we touched it, and can delay the timer to hide

- (void)_handlePanGesture:(id)arg1
{
    %orig(arg1);
    didTouchRatingControl = YES;
}

- (void)_handleTapGesture:(id)arg1
{
    %orig(arg1);
    didTouchRatingControl = YES;
}

%end




