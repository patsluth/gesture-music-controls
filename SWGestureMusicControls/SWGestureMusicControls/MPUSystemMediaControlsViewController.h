//
//  MPUSystemMediaControlsViewController.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@interface MPUSystemMediaControlsViewController : UIViewController
{
    //MPUNowPlayingController *_nowPlayingController;
    //MPVolumeController *_volumeController;
    //MPAudioDeviceController *_audioDeviceController;
    _MPUSystemMediaControlsView *_mediaControlsView;
}

- (void)_launchCurrentNowPlayingApp;
- (void)_likeBanButtonTapped:(id)arg1;

- (void)nowPlayingController:(id)arg1 nowPlayingInfoDidChange:(id)arg2;
- (void)nowPlayingController:(id)arg1 playbackStateDidChange:(BOOL)arg2;

//new
- (void)updateGMCFrames;

@end




