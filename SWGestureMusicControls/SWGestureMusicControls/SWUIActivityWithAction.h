//
//  SWUIActivityWithAction.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-02-26.
//
//

#define IMAGE_NAME_UIACTIVITY_CROSS @"SW_GMC_UIActivity_Cross"

typedef void(^swActivityAction)();

@interface SWUIActivityWithAction : UIActivity

- (id)initWithAction:(swActivityAction)action;

@end
