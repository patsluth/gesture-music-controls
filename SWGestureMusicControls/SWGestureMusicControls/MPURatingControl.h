//
//  MPURatingControl.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@interface MPURatingControl : UIControl
{
}

- (void)_handlePanGesture:(id)arg1;
- (void)_handleTapGesture:(id)arg1;

@end




