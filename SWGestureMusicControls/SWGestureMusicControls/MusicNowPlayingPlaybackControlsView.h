//
//  MusicNowPlayingPlaybackControlsView.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

#import "MPPlaybackControlsView.h"
#import "MusicNowPlayingTransportControls.h"
#import "MPVolumeSlider.h"

@interface MusicNowPlayingPlaybackControlsView : MPPlaybackControlsView
{
	MusicNowPlayingTransportControls *_transportControls;
	MPVolumeSlider *_volumeSlider;
}

@end




