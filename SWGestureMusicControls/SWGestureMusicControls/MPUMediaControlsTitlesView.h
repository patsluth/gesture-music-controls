//
//  MPUMediaControlsTitlesView.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

#import "MPUNowPlayingTitlesView.h"

@interface MPUMediaControlsTitlesView : MPUNowPlayingTitlesView
{
}

- (id)initWithMediaControlsStyle:(int)arg1;
- (int)mediaControlsStyle;

@end




