//
//  MPTransportControls.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@class MPButton;

@interface MPTransportControls : UIView
{
    MPButton *_likeOrBanButton;
    
    MPButton *_nextButton;
    MPButton *_playButton;
    MPButton *_previousButton;
    
    unsigned long long _visibleParts;
}

@property unsigned long long visibleParts;

@end