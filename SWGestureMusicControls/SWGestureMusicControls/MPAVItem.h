//
//  MPAVItem.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-07-30.
//
//

@class MPMediaItem;
@class RadioTrack;

@interface MPAVItem : NSObject
{
    MPMediaItem *_mediaItem;
}

@property(readonly) NSString *mainTitle;
@property(readonly) NSString *album;
@property(readonly) NSString *albumArtist;
@property(readonly) NSString *artist;

@property(readonly) NSString *genre;
@property(getter = isExplicitTrack, readonly) BOOL explicitTrack;

@property(readonly) BOOL isRadioItem;

@property(readonly) MPMediaItem *mediaItem;
@property(readonly) RadioTrack *radioTrack;

@property(readonly) BOOL supportsLikeOrBan;
@property(readonly) BOOL supportsShare;
@property(readonly) BOOL supportsSkip;
@property(readonly) BOOL supportsTrackInfo;

@property(readonly) float userRating;

@end




