//
//  SWGMCPrefsBundleController.h
//  SWGMCPrefsBundle
//
//  Created by Pat Sluth on 2/24/2014.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Preferences/Preferences.h>
#import <MessageUI/MessageUI.h>

#define SW_GMC_PREFERENCES_PATH @"/User/Library/Preferences/com.patsluth.SWGMCPrefsBundle2.0.plist"
#define SW_GMC_PREFS_CHANGED_NOTIFICATION "com.patsluth.SWGMCPrefsBundle2.0.settingsChanged"

@interface SWGMCPrefsBundleController : PSListController <MFMailComposeViewControllerDelegate>
{
}

- (id)getVersionNumberForSpecifier:(PSSpecifier *)specifier;

- (void)viewTutorialVideo:(PSSpecifier *)specifier;
- (void)followOnTwitter:(PSSpecifier *)specifier;
- (void)sendEmail:(PSSpecifier *)specifier;
- (void)visitWebSite:(PSSpecifier *)specifier;

@end




