//
//  SWGMCPrefsActionsController.m
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-03-08.
//
//

#import "SWGMCPrefsActionsController.h"
#import "SWGMCPrefsInstanceController.h"
#import "SWGMCPrefsBundleController.h"

#import <Preferences/PSTableCell.h>

#import "SWUIAlertView.h"

//this will only allow one action per all gestures if set to YES
//otherwise you can use multiple actions (tap - play/pause, swipe up - play/pause)
#define FORCE_SINGLE_ACTION NO

@interface SWGMCPrefsActionsController()
{
}

@end

@implementation SWGMCPrefsActionsController

- (NSArray *)validActionTitles
{
    return @[@"None",
             @"Play/Pause",
             @"Previous Song",
             @"Next Song",
             @"Back 30s",
             @"Forward 30s",
             @"Open Activity",
             @"Open App",
             @"Show Ratings",
             @"Decrease Volume",
             @"Increase Volume"];
}

- (NSArray *)validActionValues
{
    return @[@0,
             @1,
             @2,
             @3,
             @4,
             @5,
             @6,
             @7,
             @8,
             @9,
             @10];
}

- (NSString *)actionTitleForValue:(NSString *)value
{
    return [[self validActionTitles] objectAtIndex:[[self validActionValues] indexOfObject:value]];
}

- (NSString *)actionValueForTitle:(NSString *)title
{
    return [[self validActionValues] objectAtIndex:[[self validActionTitles] indexOfObject:title]];
}

- (void)applyToAll:(PSSpecifier *)specifier
{
    [[[SWUIAlertView alloc] initWithTitle:@"Confirm"
                                  message:@"Are you sure you want to apply these settings to all Gesture Music Controls Instances? (Lock Screen, Control Center, Music App)"
                               completion:^(UIAlertView *uiAlert, NSInteger buttonIndex){
                                   
                                   if (buttonIndex == 0){ //Cancel
                                       
                                   } else { //Yes
                                       
                                       for (PSSpecifier *spec in self.specifiers){
                                           if ([self isValidActionSpec:spec]){
                                               
                                               //generate all keys we need to save
                                               NSString *originalKey = [spec.properties valueForKey:@"key"];
                                               NSString *basekey = [spec.properties valueForKey:@"basekey"];
                                               
                                               for (NSString *keyPrefix in [SWGMCPrefsInstanceController allKeyPrefixes]){
                                                   
                                                   NSString *newKey = [NSString stringWithFormat:@"%@%@",
                                                                       keyPrefix,
                                                                       basekey];
                                                   [spec.properties setValue:newKey forKey:@"key"];
                                                   [PSRootController writePreference:spec];
                                               }
                                               
                                               //revert these specifiers to original key values
                                               [spec.properties setValue:originalKey forKey:@"key"];
                                           }
                                       }
                                   }
                               }
                        cancelButtonTitle:@"Cancel"
                        otherButtonTitles:@"Yes", nil] show];
}

- (void)setPreferenceValue:(id)value specifier:(PSSpecifier *)specifier
{
    if ([value isEqual:@0]){
        [super setPreferenceValue:value specifier:specifier];
        return;
    }
    
    void (^overwriteActionForSpecifier)(PSSpecifier *spec) = ^(PSSpecifier *spec){
        if ([self isValidActionSpec:specifier]){
            
            [super setPreferenceValue:@0 specifier:spec];
            
            PSTableCell *cell = [spec.properties valueForKey:@"cellObject"];
            [cell refreshCellContentsWithSpecifier:spec];
        }
    };
    
    if (FORCE_SINGLE_ACTION){
        for (PSSpecifier *spec in self.specifiers){
            if (spec != specifier && [self isValidActionSpec:spec]){
                
                if ([[spec.properties valueForKey:@"value"] isEqual:value]){
                    
                    [[[SWUIAlertView alloc] initWithTitle:@"Action In Use"
                                                  message:@"Do you want to overwrite the current action?"
                                               completion:^(UIAlertView *alert, NSInteger buttonIndex){
                                                   
                                                   if (buttonIndex == 0){ //Cancel
                                                       [super setPreferenceValue:[specifier.properties
                                                                     valueForKey:@"value"] specifier:specifier];
                                                       PSListItemsController *vc = [self.rootController.viewControllers
                                                                                    lastObject];
                                                       
                                                       NSInteger row = [[self validActionValues]
                                                                        indexOfObject:[specifier.properties
                                                                                       valueForKey:@"value"]];
                                                       
                                                       NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row
                                                                                                   inSection:0];
                                                       
                                                       [vc.table selectRowAtIndexPath:indexPath
                                                                             animated:NO
                                                                       scrollPosition:UITableViewScrollPositionNone];
                                                       [vc reload];
                                                       [vc reloadSpecifiers];
                                                       
                                                       return;
                                                   } else if (buttonIndex == 1){ //Yes
                                                       overwriteActionForSpecifier(spec);
                                                       [super setPreferenceValue:value specifier:specifier];
                                                       return;
                                                   }
                                                   
                                               } cancelButtonTitle:@"Cancel"
                                        otherButtonTitles:@"Yes", nil] show];
                    
                    return;
                    
                } else {
                }
            }
        }
    }
    
    //if we hit this there were no conflicting actions and we can set it normally
    [super setPreferenceValue:value specifier:specifier];
}

- (BOOL)isValidActionSpec:(PSSpecifier *)specifier
{
    NSString *actionTitles = NSStringFromSelector(@selector(validActionTitles));
    NSString *actionValues = NSStringFromSelector(@selector(validActionValues));
    
    return ([[specifier.properties
              valueForKey:@"titlesDataSource"]
             isEqualToString:actionTitles] && [[specifier.properties
                                                valueForKey:@"valuesDataSource"]
                                               isEqualToString:actionValues]);
}

- (id)specifiers
{
	if (_specifiers == nil) {
		_specifiers = [self loadSpecifiersFromPlistName:@"SWGMCPrefsActions" target:self];
        
        //append our prefix so we can differentiate between the different instances
        if ([[self.parentController class] isSubclassOfClass:[SWGMCPrefsInstanceController class]]){
            
            for (PSSpecifier *spec in _specifiers){
                
                if ([self isValidActionSpec:spec]){
                    NSString *basekey = [spec.properties valueForKey:@"basekey"];
                    NSString *newKey = [NSString stringWithFormat:@"%@%@", [[self.parentController class]
                                                                            keyPrefix], basekey];
                    [spec.properties setValue:newKey forKey:@"key"];
                }
            }
        }
	}
	
	return _specifiers;
}

@end




