//
//  SWGMCPrefsInstanceController.m
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-03-08.
//
//

#import "SWGMCPrefsInstanceController.h"

#import "SWGMCPrefsLockScreenController.h"
#import "SWGMCPrefsControlCenterController.h"
#import "SWGMCPrefsMusicAppController.h"

@implementation SWGMCPrefsInstanceController

- (void)setSpecifier:(PSSpecifier *)specifier
{
    [super setSpecifier:specifier];
    
    [self setTitle:specifier.name];
}

- (void)setTitle:(NSString *)title
{
    if (self.specifier && self.specifier.name){
        [super setTitle:self.specifier.name];
        return;
    }
    
    [super setTitle:title];
}

- (void)_returnKeyPressed:(id)pressed
{
    [super _returnKeyPressed:pressed];
    
    //this will dismiss the keyboard and save the preferences for the selected text field
    if ([self isKindOfClass:[UIViewController class]]){
        UIViewController *vcSelf = (UIViewController *)self;
        [vcSelf.view endEditing:YES];
    }
}

- (id)specifiers
{
	if (_specifiers == nil) {
        //self.parentController = SWGMCPrefsBundleController so value change will call setPreferenceValue:spec:
		_specifiers = [self loadSpecifiersFromPlistName:@"SWGMCPrefsInstance" target:self.parentController];
	}
    
    for (PSSpecifier *spec in _specifiers){
        
        NSString *baseKey = [spec.properties valueForKey:@"basekey"];
        NSString *key = [spec.properties valueForKey:@"key"];
        
        if (baseKey && [key isEqualToString:@""]){
            NSString *newKey = [NSString stringWithFormat:@"%@%@", [[self class] keyPrefix], baseKey];
            [spec.properties setValue:newKey forKey:@"key"];
        }
    }
	
	return _specifiers;
}

+ (NSArray *)allKeyPrefixes
{
    return @[[SWGMCPrefsLockScreenController keyPrefix],
             [SWGMCPrefsControlCenterController keyPrefix],
             [SWGMCPrefsMusicAppController keyPrefix]];
}

+ (NSString *)keyPrefix
{
    return @"";
}

@end
