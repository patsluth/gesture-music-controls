//
//  SWGMCPrefsInstanceController.h
//  SWGestureMusicControls
//
//  Created by Pat Sluth on 2014-03-08.
//
//

#import <Preferences/Preferences.h>

@interface SWGMCPrefsInstanceController : PSListController

+ (NSArray *)allKeyPrefixes;
+ (NSString *)keyPrefix;

@end
