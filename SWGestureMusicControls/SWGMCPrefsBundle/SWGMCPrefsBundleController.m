//
//  SWGMCPrefsBundleController.m
//  SWGMCPrefsBundle
//
//  Created by Pat Sluth on 2/24/2014.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SWGMCPrefsBundleController.h"
#import "SWGMCPrefsInstanceController.h"

#import <libpackageinfo/libpackageinfo.h>

#define DPKG_STATUS_PATH @"/var/lib/dpkg/status"

#define SW_RESPRING_SPECIFIER_ID @"Respring"

@interface SWGMCPrefsBundleController()
{
}

@property (strong, nonatomic) NSMutableArray *respringValues;

- (void)updateRespringButton;
- (void)respring:(PSSpecifier *)specifier;

@property (strong, nonatomic) NSDictionary *originalPreferences;
@property (strong, nonatomic) PIDebianPackage *packageDEB;

@end

@implementation SWGMCPrefsBundleController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.originalPreferences = [[NSDictionary alloc] initWithContentsOfFile:SW_GMC_PREFERENCES_PATH];
    self.packageDEB = self.packageDEB; //cache on load
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
    dispatch_async(queue, ^{
        
        self.packageDEB = [PIDebianPackage packageForFile:SW_GMC_BUNDLE_PATH];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            PSSpecifier *spec = [self specifierForID:@"Version"];
            if (spec){
                [self reloadSpecifier:spec animated:YES];
            }
            
        });
    });
}

#pragma mark Preferences

- (id)getVersionNumberForSpecifier:(PSSpecifier *)specifier
{
    if (self.packageDEB){
        return self.packageDEB.version;
    }
    
    return @"...";
}

- (void)setPreferenceValue:(id)value specifier:(PSSpecifier *)specifier
{
    [self _returnKeyPressed:nil];
    
    NSString *specifierKey = [specifier.properties objectForKey:@"key"];
    
    id currentSavedValue = [self.originalPreferences objectForKey:specifierKey];
    id defaultValue = [specifier.properties objectForKey:@"default"];
    
    //if this doesnt exist, it means it requires a respring to take effect. BITCH
    id postNotification = [specifier.properties objectForKey:@"PostNotification"];
    
    if (!postNotification){
        if (currentSavedValue){
            if ([value isEqual:currentSavedValue]){
                if ([self.respringValues containsObject:specifierKey]){
                    [self.respringValues removeObject:specifierKey];
                }
            } else {
                if (![self.respringValues containsObject:specifierKey]){
                    [self.respringValues addObject:specifierKey];
                }
            }
        } else { //check defaults
            if ([value isEqual:defaultValue]){
                if ([self.respringValues containsObject:specifierKey]){
                    [self.respringValues removeObject:specifierKey];
                }
            } else {
                if (![self.respringValues containsObject:specifierKey]){
                    [self.respringValues addObject:specifierKey];
                }
            }
        }
    } else {
        if ([self.respringValues containsObject:specifierKey]){
            [self.respringValues removeObject:specifierKey];
        }
    }
    
    [self updateRespringButton];
    
    [super setPreferenceValue:value specifier:specifier];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark Respring

- (void)updateRespringButton
{
    if (self.respringValues.count > 0){
        //show button
        if (![self specifierForID:SW_RESPRING_SPECIFIER_ID]){
            PSSpecifier *respring = [PSSpecifier preferenceSpecifierNamed:SW_RESPRING_SPECIFIER_ID
                                                                   target:self
                                                                      set:nil
                                                                      get:nil
                                                                   detail:nil
                                                                     cell:[PSTableCell cellTypeFromString:@"PSButtonCell"]
                                                                     edit:nil];
            respring->action = @selector(respring:);
            [self insertSpecifier:respring atIndex:0 animated:YES];
        }
    } else {
        //remove button
        if ([self specifierForID:SW_RESPRING_SPECIFIER_ID]){
            [self removeSpecifierAtIndex:0 animated:YES];
        }
    }
}

- (void)respring:(PSSpecifier *)specifier
{
    [self suspend];
    system("killall SpringBoard");
}

#pragma mark Button Cell Actions

- (void)viewTutorialVideo:(PSSpecifier *)specifier
{
    //random string so php doesnt cache
    NSInteger randomStringLength = 200;
    NSMutableString *randomString = [NSMutableString stringWithCapacity:randomStringLength];
    for (int i = 0; i < randomStringLength; i++){
        [randomString appendFormat:@"%C", (unichar)('a' + arc4random_uniform(25))];
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@",
                     @"http://sluthware.com/SluthwareApps/SWGMCTutorialVideoURL.php?",
                     randomString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:url]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError){
                                if (connectionError){
                                    //NSLog(@"SW Error - %@", connectionError);
                                    [[[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"Could not retrive Video URL"
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil, nil] show];
                                } else {
                                    NSString *result = [[NSString alloc] initWithData:data
                                                                             encoding:NSUTF8StringEncoding];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:result]];
                                }
                           }];
}

- (void)followOnTwitter:(PSSpecifier *)specifier
{
    NSURL *tweetbotURL = [NSURL URLWithString:@"tweetbot:///user_profile/patsluth"];
    NSURL *twitterURL = [NSURL URLWithString:@"twitter://user?screen_name=patsluth"];
    
    if ([[UIApplication sharedApplication] canOpenURL:tweetbotURL]){
        [[UIApplication sharedApplication] openURL:tweetbotURL];
        return;
    } else if ([[UIApplication sharedApplication] canOpenURL:twitterURL]){
        [[UIApplication sharedApplication] openURL:twitterURL];
        return;
    }
    
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/patsluth"]];
}

- (void)sendEmail:(PSSpecifier *)specifier
{
    MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
    
    mailController.mailComposeDelegate = self;
    
    NSString *subject = @"Gesture Music Controls Inquiry";
    
    if (self.packageDEB){
        subject = [NSString stringWithFormat:@"%@ %@", self.packageDEB.name, self.packageDEB.version];
    }
    
    [mailController setSubject:subject];
    [mailController setToRecipients:@[@"pat.sluth@gmail.com"]];
    
    NSError *error;
    NSData *attachment = [NSData dataWithContentsOfFile:DPKG_STATUS_PATH options:NSDataReadingUncached error:&error];
    
    if (!error && attachment){
        [mailController addAttachmentData:attachment mimeType:@"" fileName:@"cydialog"];
    }
    
    UIViewController *vc = (UIViewController *)self.parentController;
    [vc presentViewController:mailController animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    UIViewController *vc = (UIViewController *)self.parentController;
    [vc dismissViewControllerAnimated:YES completion:nil];
}

- (void)visitWebSite:(PSSpecifier *)specifier
{
    [[[UIAlertView alloc] initWithTitle:@"Not Available"
                                message:@"Website is coming soon"
                               delegate:nil
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil, nil] show];
    
	//[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.sluthware.com"]];
}

#pragma mark Helper and Cleanup

- (id)specifiers
{
	if (_specifiers == nil){
		_specifiers = [self loadSpecifiersFromPlistName:@"SWGMCPrefsBundle" target:self];
	}
	
	return _specifiers;
}

- (NSMutableArray *)respringValues
{
    if (!_respringValues){
        _respringValues = [[NSMutableArray alloc] init];
    }
    return _respringValues;
}

- (void)dealloc
{
    [_respringValues removeAllObjects];
    _respringValues = nil;
}

@end




